/********************************************************************************
** Form generated from reading UI file 'logorregwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGORREGWINDOW_H
#define UI_LOGORREGWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LogOrRegWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QLabel *fish_pix;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *log_btn;
    QPushButton *reg_btn;
    QPushButton *exit_btn;

    void setupUi(QMainWindow *LogOrRegWindow)
    {
        if (LogOrRegWindow->objectName().isEmpty())
            LogOrRegWindow->setObjectName(QStringLiteral("LogOrRegWindow"));
        LogOrRegWindow->resize(419, 275);
        LogOrRegWindow->setStyleSheet(QLatin1String("background-color: rgb(0, 170, 255);\n"
"background-color: rgb(155, 222, 255);"));
        centralwidget = new QWidget(LogOrRegWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(20);
        font.setItalic(true);
        label->setFont(font);
        label->setStyleSheet(QLatin1String("\n"
"color: rgb(0, 0, 127);"));

        horizontalLayout_3->addWidget(label);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        fish_pix = new QLabel(centralwidget);
        fish_pix->setObjectName(QStringLiteral("fish_pix"));

        horizontalLayout_4->addWidget(fish_pix);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_4);


        horizontalLayout->addLayout(verticalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        log_btn = new QPushButton(centralwidget);
        log_btn->setObjectName(QStringLiteral("log_btn"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(log_btn->sizePolicy().hasHeightForWidth());
        log_btn->setSizePolicy(sizePolicy);
        log_btn->setCursor(QCursor(Qt::PointingHandCursor));
        log_btn->setMouseTracking(false);
        log_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout_2->addWidget(log_btn);

        reg_btn = new QPushButton(centralwidget);
        reg_btn->setObjectName(QStringLiteral("reg_btn"));
        sizePolicy.setHeightForWidth(reg_btn->sizePolicy().hasHeightForWidth());
        reg_btn->setSizePolicy(sizePolicy);
        reg_btn->setCursor(QCursor(Qt::PointingHandCursor));
        reg_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout_2->addWidget(reg_btn);

        exit_btn = new QPushButton(centralwidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        exit_btn->setCursor(QCursor(Qt::PointingHandCursor));
        exit_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout_2->addWidget(exit_btn);


        horizontalLayout_2->addLayout(verticalLayout_2);


        horizontalLayout->addLayout(horizontalLayout_2);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        LogOrRegWindow->setCentralWidget(centralwidget);

        retranslateUi(LogOrRegWindow);

        QMetaObject::connectSlotsByName(LogOrRegWindow);
    } // setupUi

    void retranslateUi(QMainWindow *LogOrRegWindow)
    {
        LogOrRegWindow->setWindowTitle(QApplication::translate("LogOrRegWindow", "Aqua Table", 0));
        label->setText(QApplication::translate("LogOrRegWindow", "Aqua Table", 0));
        fish_pix->setText(QString());
        log_btn->setText(QApplication::translate("LogOrRegWindow", "Log in", 0));
        reg_btn->setText(QApplication::translate("LogOrRegWindow", "Register", 0));
        exit_btn->setText(QApplication::translate("LogOrRegWindow", "Exit", 0));
    } // retranslateUi

};

namespace Ui {
    class LogOrRegWindow: public Ui_LogOrRegWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGORREGWINDOW_H
