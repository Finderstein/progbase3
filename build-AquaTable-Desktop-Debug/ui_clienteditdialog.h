/********************************************************************************
** Form generated from reading UI file 'clienteditdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTEDITDIALOG_H
#define UI_CLIENTEDITDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ClientEditDialog
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label_9;
    QSpacerItem *horizontalSpacer_2;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *usern_line;
    QLabel *label_2;
    QLineEdit *pas_line;
    QLabel *label_3;
    QLabel *name_label;
    QLabel *label_4;
    QLabel *sur_label;
    QLabel *label_5;
    QLineEdit *phone_line;
    QLabel *err_label;

    void setupUi(QDialog *ClientEditDialog)
    {
        if (ClientEditDialog->objectName().isEmpty())
            ClientEditDialog->setObjectName(QStringLiteral("ClientEditDialog"));
        ClientEditDialog->resize(483, 359);
        ClientEditDialog->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        gridLayout = new QGridLayout(ClientEditDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonBox = new QDialogButtonBox(ClientEditDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setCursor(QCursor(Qt::PointingHandCursor));
        buttonBox->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_9 = new QLabel(ClientEditDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setStyleSheet(QStringLiteral("color: rgb(0, 0, 0);"));

        horizontalLayout->addWidget(label_9);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(ClientEditDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        usern_line = new QLineEdit(ClientEditDialog);
        usern_line->setObjectName(QStringLiteral("usern_line"));
        usern_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(0, QFormLayout::FieldRole, usern_line);

        label_2 = new QLabel(ClientEditDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        pas_line = new QLineEdit(ClientEditDialog);
        pas_line->setObjectName(QStringLiteral("pas_line"));
        pas_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(1, QFormLayout::FieldRole, pas_line);

        label_3 = new QLabel(ClientEditDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        name_label = new QLabel(ClientEditDialog);
        name_label->setObjectName(QStringLiteral("name_label"));

        formLayout->setWidget(2, QFormLayout::FieldRole, name_label);

        label_4 = new QLabel(ClientEditDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        sur_label = new QLabel(ClientEditDialog);
        sur_label->setObjectName(QStringLiteral("sur_label"));

        formLayout->setWidget(3, QFormLayout::FieldRole, sur_label);

        label_5 = new QLabel(ClientEditDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        phone_line = new QLineEdit(ClientEditDialog);
        phone_line->setObjectName(QStringLiteral("phone_line"));
        phone_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        phone_line->setMaxLength(12);

        formLayout->setWidget(4, QFormLayout::FieldRole, phone_line);


        verticalLayout->addLayout(formLayout);

        err_label = new QLabel(ClientEditDialog);
        err_label->setObjectName(QStringLiteral("err_label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(err_label->sizePolicy().hasHeightForWidth());
        err_label->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(err_label);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(ClientEditDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ClientEditDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ClientEditDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ClientEditDialog);
    } // setupUi

    void retranslateUi(QDialog *ClientEditDialog)
    {
        ClientEditDialog->setWindowTitle(QApplication::translate("ClientEditDialog", "Aqua Table", 0));
        label_9->setText(QApplication::translate("ClientEditDialog", "Edit your profile", 0));
        label->setText(QApplication::translate("ClientEditDialog", "Username:", 0));
        label_2->setText(QApplication::translate("ClientEditDialog", "Change password:", 0));
        label_3->setText(QApplication::translate("ClientEditDialog", "Name:", 0));
        name_label->setText(QString());
        label_4->setText(QApplication::translate("ClientEditDialog", "Surname", 0));
        sur_label->setText(QString());
        label_5->setText(QApplication::translate("ClientEditDialog", "Phone:", 0));
        err_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ClientEditDialog: public Ui_ClientEditDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTEDITDIALOG_H
