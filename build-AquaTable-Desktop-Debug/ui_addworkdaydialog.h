/********************************************************************************
** Form generated from reading UI file 'addworkdaydialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDWORKDAYDIALOG_H
#define UI_ADDWORKDAYDIALOG_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AddWorkdayDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label_9;
    QSpacerItem *horizontalSpacer_2;
    QFormLayout *formLayout;
    QLabel *label;
    QDateEdit *date;
    QLabel *label_2;
    QTimeEdit *open_timeEdit;
    QLabel *label_3;
    QTimeEdit *close_timeEdit;
    QLabel *label_4;
    QLabel *label_5;
    QSpinBox *under18_spinBox;
    QLabel *label_6;
    QSpinBox *under12_spinBox;
    QSpinBox *adult_spinBox;
    QLabel *err_label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AddWorkdayDialog)
    {
        if (AddWorkdayDialog->objectName().isEmpty())
            AddWorkdayDialog->setObjectName(QStringLiteral("AddWorkdayDialog"));
        AddWorkdayDialog->resize(428, 381);
        AddWorkdayDialog->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        gridLayout = new QGridLayout(AddWorkdayDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_9 = new QLabel(AddWorkdayDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setStyleSheet(QStringLiteral("color: rgb(0, 0, 0);"));

        horizontalLayout->addWidget(label_9);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(AddWorkdayDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        date = new QDateEdit(AddWorkdayDialog);
        date->setObjectName(QStringLiteral("date"));
        date->setCursor(QCursor(Qt::PointingHandCursor));
        date->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        date->setMinimumDate(QDate(2018, 6, 1));
        date->setCalendarPopup(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, date);

        label_2 = new QLabel(AddWorkdayDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        open_timeEdit = new QTimeEdit(AddWorkdayDialog);
        open_timeEdit->setObjectName(QStringLiteral("open_timeEdit"));
        open_timeEdit->setCursor(QCursor(Qt::PointingHandCursor));
        open_timeEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        open_timeEdit->setMinimumTime(QTime(6, 0, 0));

        formLayout->setWidget(1, QFormLayout::FieldRole, open_timeEdit);

        label_3 = new QLabel(AddWorkdayDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        close_timeEdit = new QTimeEdit(AddWorkdayDialog);
        close_timeEdit->setObjectName(QStringLiteral("close_timeEdit"));
        close_timeEdit->setCursor(QCursor(Qt::PointingHandCursor));
        close_timeEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(2, QFormLayout::FieldRole, close_timeEdit);

        label_4 = new QLabel(AddWorkdayDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(AddWorkdayDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        under18_spinBox = new QSpinBox(AddWorkdayDialog);
        under18_spinBox->setObjectName(QStringLiteral("under18_spinBox"));
        under18_spinBox->setCursor(QCursor(Qt::PointingHandCursor));
        under18_spinBox->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        under18_spinBox->setMaximum(9999);

        formLayout->setWidget(4, QFormLayout::FieldRole, under18_spinBox);

        label_6 = new QLabel(AddWorkdayDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        under12_spinBox = new QSpinBox(AddWorkdayDialog);
        under12_spinBox->setObjectName(QStringLiteral("under12_spinBox"));
        under12_spinBox->setCursor(QCursor(Qt::PointingHandCursor));
        under12_spinBox->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        under12_spinBox->setMaximum(9999);

        formLayout->setWidget(5, QFormLayout::FieldRole, under12_spinBox);

        adult_spinBox = new QSpinBox(AddWorkdayDialog);
        adult_spinBox->setObjectName(QStringLiteral("adult_spinBox"));
        adult_spinBox->setCursor(QCursor(Qt::PointingHandCursor));
        adult_spinBox->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        adult_spinBox->setMaximum(9999);

        formLayout->setWidget(3, QFormLayout::FieldRole, adult_spinBox);


        verticalLayout->addLayout(formLayout);

        err_label = new QLabel(AddWorkdayDialog);
        err_label->setObjectName(QStringLiteral("err_label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(err_label->sizePolicy().hasHeightForWidth());
        err_label->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(err_label);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(AddWorkdayDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setCursor(QCursor(Qt::PointingHandCursor));
        buttonBox->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);


        retranslateUi(AddWorkdayDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AddWorkdayDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AddWorkdayDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(AddWorkdayDialog);
    } // setupUi

    void retranslateUi(QDialog *AddWorkdayDialog)
    {
        AddWorkdayDialog->setWindowTitle(QApplication::translate("AddWorkdayDialog", "Aqua Table", 0));
        label_9->setText(QApplication::translate("AddWorkdayDialog", "Add new workday", 0));
        label->setText(QApplication::translate("AddWorkdayDialog", "Date:", 0));
        date->setDisplayFormat(QApplication::translate("AddWorkdayDialog", "dd.MM.yyyy", 0));
        label_2->setText(QApplication::translate("AddWorkdayDialog", "Open time:", 0));
        label_3->setText(QApplication::translate("AddWorkdayDialog", "Close time:", 0));
        label_4->setText(QApplication::translate("AddWorkdayDialog", "Price for adults:", 0));
        label_5->setText(QApplication::translate("AddWorkdayDialog", "<html><head/><body><p>Price under 18:</p></body></html>", 0));
        label_6->setText(QApplication::translate("AddWorkdayDialog", "Price under 12:", 0));
        err_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class AddWorkdayDialog: public Ui_AddWorkdayDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDWORKDAYDIALOG_H
