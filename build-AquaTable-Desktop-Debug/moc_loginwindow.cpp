/****************************************************************************
** Meta object code from reading C++ file 'loginwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../AquaTable/loginwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'loginwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_LogInWindow_t {
    QByteArrayData data[13];
    char stringdata0[175];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LogInWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LogInWindow_t qt_meta_stringdata_LogInWindow = {
    {
QT_MOC_LITERAL(0, 0, 11), // "LogInWindow"
QT_MOC_LITERAL(1, 12, 10), // "sendClient"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 7), // "Client*"
QT_MOC_LITERAL(4, 32, 10), // "sendWorker"
QT_MOC_LITERAL(5, 43, 7), // "Worker*"
QT_MOC_LITERAL(6, 51, 10), // "sendStatus"
QT_MOC_LITERAL(7, 62, 4), // "stat"
QT_MOC_LITERAL(8, 67, 20), // "on_logIn_btn_clicked"
QT_MOC_LITERAL(9, 88, 31), // "on_password_lineEdit_textEdited"
QT_MOC_LITERAL(10, 120, 4), // "arg1"
QT_MOC_LITERAL(11, 125, 29), // "on_usname_lineEdit_textEdited"
QT_MOC_LITERAL(12, 155, 19) // "on_exit_btn_clicked"

    },
    "LogInWindow\0sendClient\0\0Client*\0"
    "sendWorker\0Worker*\0sendStatus\0stat\0"
    "on_logIn_btn_clicked\0"
    "on_password_lineEdit_textEdited\0arg1\0"
    "on_usname_lineEdit_textEdited\0"
    "on_exit_btn_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LogInWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       4,    1,   52,    2, 0x06 /* Public */,
       6,    1,   55,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   58,    2, 0x08 /* Private */,
       9,    1,   59,    2, 0x08 /* Private */,
      11,    1,   62,    2, 0x08 /* Private */,
      12,    0,   65,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 5,    2,
    QMetaType::Void, QMetaType::Int,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void,

       0        // eod
};

void LogInWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LogInWindow *_t = static_cast<LogInWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendClient((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 1: _t->sendWorker((*reinterpret_cast< Worker*(*)>(_a[1]))); break;
        case 2: _t->sendStatus((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_logIn_btn_clicked(); break;
        case 4: _t->on_password_lineEdit_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_usname_lineEdit_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_exit_btn_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Client* >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Worker* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (LogInWindow::*_t)(Client * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LogInWindow::sendClient)) {
                *result = 0;
            }
        }
        {
            typedef void (LogInWindow::*_t)(Worker * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LogInWindow::sendWorker)) {
                *result = 1;
            }
        }
        {
            typedef void (LogInWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LogInWindow::sendStatus)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject LogInWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_LogInWindow.data,
      qt_meta_data_LogInWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *LogInWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LogInWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_LogInWindow.stringdata0))
        return static_cast<void*>(const_cast< LogInWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int LogInWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void LogInWindow::sendClient(Client * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void LogInWindow::sendWorker(Worker * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void LogInWindow::sendStatus(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
