/********************************************************************************
** Form generated from reading UI file 'loginwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LogInWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLineEdit *usname_lineEdit;
    QLabel *paswrd_label;
    QPushButton *exit_btn;
    QLabel *err_label;
    QLabel *usname_label;
    QPushButton *logIn_btn;
    QLineEdit *password_lineEdit;
    QLabel *fish_pix;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *LogInWindow)
    {
        if (LogInWindow->objectName().isEmpty())
            LogInWindow->setObjectName(QStringLiteral("LogInWindow"));
        LogInWindow->setEnabled(true);
        LogInWindow->resize(480, 235);
        LogInWindow->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        LogInWindow->setAnimated(true);
        LogInWindow->setDocumentMode(false);
        LogInWindow->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(LogInWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        usname_lineEdit = new QLineEdit(centralWidget);
        usname_lineEdit->setObjectName(QStringLiteral("usname_lineEdit"));
        usname_lineEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(usname_lineEdit, 2, 0, 1, 1);

        paswrd_label = new QLabel(centralWidget);
        paswrd_label->setObjectName(QStringLiteral("paswrd_label"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(paswrd_label->sizePolicy().hasHeightForWidth());
        paswrd_label->setSizePolicy(sizePolicy);

        gridLayout->addWidget(paswrd_label, 3, 0, 1, 1);

        exit_btn = new QPushButton(centralWidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(exit_btn->sizePolicy().hasHeightForWidth());
        exit_btn->setSizePolicy(sizePolicy1);
        exit_btn->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon;
        icon.addFile(QStringLiteral("../icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        exit_btn->setIcon(icon);
        exit_btn->setIconSize(QSize(50, 35));
        exit_btn->setFlat(true);

        gridLayout->addWidget(exit_btn, 0, 0, 1, 1);

        err_label = new QLabel(centralWidget);
        err_label->setObjectName(QStringLiteral("err_label"));
        err_label->setStyleSheet(QStringLiteral("color: rgb(170, 0, 0);"));

        gridLayout->addWidget(err_label, 5, 0, 1, 1);

        usname_label = new QLabel(centralWidget);
        usname_label->setObjectName(QStringLiteral("usname_label"));
        sizePolicy.setHeightForWidth(usname_label->sizePolicy().hasHeightForWidth());
        usname_label->setSizePolicy(sizePolicy);

        gridLayout->addWidget(usname_label, 1, 0, 1, 1);

        logIn_btn = new QPushButton(centralWidget);
        logIn_btn->setObjectName(QStringLiteral("logIn_btn"));
        logIn_btn->setEnabled(false);
        logIn_btn->setCursor(QCursor(Qt::PointingHandCursor));
        logIn_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(logIn_btn, 5, 1, 1, 1);

        password_lineEdit = new QLineEdit(centralWidget);
        password_lineEdit->setObjectName(QStringLiteral("password_lineEdit"));
        password_lineEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        password_lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(password_lineEdit, 4, 0, 1, 1);

        fish_pix = new QLabel(centralWidget);
        fish_pix->setObjectName(QStringLiteral("fish_pix"));

        gridLayout->addWidget(fish_pix, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        LogInWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(LogInWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 480, 25));
        LogInWindow->setMenuBar(menuBar);

        retranslateUi(LogInWindow);

        QMetaObject::connectSlotsByName(LogInWindow);
    } // setupUi

    void retranslateUi(QMainWindow *LogInWindow)
    {
        LogInWindow->setWindowTitle(QApplication::translate("LogInWindow", "Aqua Table", 0));
        paswrd_label->setText(QApplication::translate("LogInWindow", "Password", 0));
        exit_btn->setText(QString());
        err_label->setText(QString());
        usname_label->setText(QApplication::translate("LogInWindow", "Username", 0));
        logIn_btn->setText(QApplication::translate("LogInWindow", "Log In", 0));
        fish_pix->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class LogInWindow: public Ui_LogInWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
