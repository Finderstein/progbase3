/********************************************************************************
** Form generated from reading UI file 'clienthistorywindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTHISTORYWINDOW_H
#define UI_CLIENTHISTORYWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientHistoryWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QPushButton *rem_btn;
    QSpacerItem *horizontalSpacer;
    QTableWidget *history_table;

    void setupUi(QMainWindow *ClientHistoryWindow)
    {
        if (ClientHistoryWindow->objectName().isEmpty())
            ClientHistoryWindow->setObjectName(QStringLiteral("ClientHistoryWindow"));
        ClientHistoryWindow->resize(720, 299);
        centralwidget = new QWidget(ClientHistoryWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        rem_btn = new QPushButton(centralwidget);
        rem_btn->setObjectName(QStringLiteral("rem_btn"));
        rem_btn->setEnabled(false);

        gridLayout->addWidget(rem_btn, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(608, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 1, 1, 1);

        history_table = new QTableWidget(centralwidget);
        if (history_table->columnCount() < 7)
            history_table->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        history_table->setObjectName(QStringLiteral("history_table"));
        history_table->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        history_table->setSortingEnabled(true);

        gridLayout->addWidget(history_table, 1, 0, 1, 2);

        ClientHistoryWindow->setCentralWidget(centralwidget);

        retranslateUi(ClientHistoryWindow);

        QMetaObject::connectSlotsByName(ClientHistoryWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ClientHistoryWindow)
    {
        ClientHistoryWindow->setWindowTitle(QApplication::translate("ClientHistoryWindow", "MainWindow", 0));
        rem_btn->setText(QApplication::translate("ClientHistoryWindow", "Remove", 0));
        QTableWidgetItem *___qtablewidgetitem = history_table->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ClientHistoryWindow", "Username", 0));
        QTableWidgetItem *___qtablewidgetitem1 = history_table->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ClientHistoryWindow", "Name", 0));
        QTableWidgetItem *___qtablewidgetitem2 = history_table->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("ClientHistoryWindow", "Surname", 0));
        QTableWidgetItem *___qtablewidgetitem3 = history_table->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("ClientHistoryWindow", "Phone", 0));
        QTableWidgetItem *___qtablewidgetitem4 = history_table->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("ClientHistoryWindow", "Date of visit", 0));
        QTableWidgetItem *___qtablewidgetitem5 = history_table->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("ClientHistoryWindow", "Start time visit", 0));
        QTableWidgetItem *___qtablewidgetitem6 = history_table->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("ClientHistoryWindow", "End time visit", 0));
    } // retranslateUi

};

namespace Ui {
    class ClientHistoryWindow: public Ui_ClientHistoryWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTHISTORYWINDOW_H
