/********************************************************************************
** Form generated from reading UI file 'clientwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTWINDOW_H
#define UI_CLIENTWINDOW_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QFrame *line_2;
    QFrame *line;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *exit_btn;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QLabel *user_label;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLabel *name_label;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QLabel *sur_label;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_5;
    QLabel *phone_label;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *edit_btn;
    QPushButton *history_btn;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_11;
    QLabel *adult_label;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_12;
    QLabel *child18_label;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_13;
    QLabel *child12_label;
    QLabel *err_label;
    QCalendarWidget *calendarWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_7;
    QLabel *clientsnum_label;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_8;
    QLabel *workTime_label;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_16;
    QSpacerItem *horizontalSpacer_11;
    QLabel *label_15;
    QSpacerItem *horizontalSpacer_12;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_17;
    QTimeEdit *startV_timeEdit;
    QHBoxLayout *horizontalLayout_17;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_9;
    QSpacerItem *horizontalSpacer_14;
    QTimeEdit *endV_timeEdit;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *add_btn;
    QSpacerItem *horizontalSpacer_10;
    QFrame *line_3;

    void setupUi(QMainWindow *ClientWindow)
    {
        if (ClientWindow->objectName().isEmpty())
            ClientWindow->setObjectName(QStringLiteral("ClientWindow"));
        ClientWindow->resize(726, 617);
        ClientWindow->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        centralwidget = new QWidget(ClientWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout_2 = new QGridLayout(centralwidget);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 0, 1, 1, 1);

        line = new QFrame(centralwidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 1, 0, 1, 3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        exit_btn = new QPushButton(centralwidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(exit_btn->sizePolicy().hasHeightForWidth());
        exit_btn->setSizePolicy(sizePolicy);
        exit_btn->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon;
        icon.addFile(QStringLiteral("../icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        exit_btn->setIcon(icon);
        exit_btn->setIconSize(QSize(50, 35));
        exit_btn->setFlat(true);

        horizontalLayout_18->addWidget(exit_btn);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        user_label = new QLabel(centralwidget);
        user_label->setObjectName(QStringLiteral("user_label"));
        user_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        user_label->setFrameShape(QFrame::WinPanel);

        horizontalLayout_4->addWidget(user_label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);


        horizontalLayout_18->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(horizontalLayout_18);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFrameShape(QFrame::NoFrame);

        horizontalLayout->addWidget(label_2);

        name_label = new QLabel(centralwidget);
        name_label->setObjectName(QStringLiteral("name_label"));
        name_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        name_label->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(name_label);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        sur_label = new QLabel(centralwidget);
        sur_label->setObjectName(QStringLiteral("sur_label"));
        sur_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        sur_label->setFrameShape(QFrame::Box);

        horizontalLayout_2->addWidget(sur_label);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_3->addWidget(label_5);

        phone_label = new QLabel(centralwidget);
        phone_label->setObjectName(QStringLiteral("phone_label"));
        phone_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        phone_label->setFrameShape(QFrame::Box);

        horizontalLayout_3->addWidget(phone_label);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        edit_btn = new QPushButton(centralwidget);
        edit_btn->setObjectName(QStringLiteral("edit_btn"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(edit_btn->sizePolicy().hasHeightForWidth());
        edit_btn->setSizePolicy(sizePolicy1);
        edit_btn->setCursor(QCursor(Qt::PointingHandCursor));
        edit_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout_5->addWidget(edit_btn);

        history_btn = new QPushButton(centralwidget);
        history_btn->setObjectName(QStringLiteral("history_btn"));
        history_btn->setCursor(QCursor(Qt::PointingHandCursor));
        history_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout_5->addWidget(history_btn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_5);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        label_10 = new QLabel(centralwidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
""));
        label_10->setFrameShape(QFrame::WinPanel);

        horizontalLayout_6->addWidget(label_10);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_11 = new QLabel(centralwidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        horizontalLayout_7->addWidget(label_11);

        adult_label = new QLabel(centralwidget);
        adult_label->setObjectName(QStringLiteral("adult_label"));
        adult_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        adult_label->setFrameShape(QFrame::Box);

        horizontalLayout_7->addWidget(adult_label);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_12 = new QLabel(centralwidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        horizontalLayout_8->addWidget(label_12);

        child18_label = new QLabel(centralwidget);
        child18_label->setObjectName(QStringLiteral("child18_label"));
        child18_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        child18_label->setFrameShape(QFrame::Box);

        horizontalLayout_8->addWidget(child18_label);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_13 = new QLabel(centralwidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_9->addWidget(label_13);

        child12_label = new QLabel(centralwidget);
        child12_label->setObjectName(QStringLiteral("child12_label"));
        child12_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        child12_label->setFrameShape(QFrame::Box);

        horizontalLayout_9->addWidget(child12_label);


        verticalLayout_2->addLayout(horizontalLayout_9);


        gridLayout->addLayout(verticalLayout_2, 0, 2, 1, 1);

        err_label = new QLabel(centralwidget);
        err_label->setObjectName(QStringLiteral("err_label"));
        err_label->setStyleSheet(QStringLiteral("color: rgb(170, 0, 0);"));

        gridLayout->addWidget(err_label, 6, 0, 1, 3);

        calendarWidget = new QCalendarWidget(centralwidget);
        calendarWidget->setObjectName(QStringLiteral("calendarWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(calendarWidget->sizePolicy().hasHeightForWidth());
        calendarWidget->setSizePolicy(sizePolicy2);
        calendarWidget->setCursor(QCursor(Qt::PointingHandCursor));
        calendarWidget->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);"));
        calendarWidget->setSelectedDate(QDate(2018, 6, 3));

        gridLayout->addWidget(calendarWidget, 5, 0, 1, 3);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_12->addWidget(label_7);

        clientsnum_label = new QLabel(centralwidget);
        clientsnum_label->setObjectName(QStringLiteral("clientsnum_label"));
        clientsnum_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        clientsnum_label->setFrameShape(QFrame::Box);

        horizontalLayout_12->addWidget(clientsnum_label);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_13->addWidget(label_8);

        workTime_label = new QLabel(centralwidget);
        workTime_label->setObjectName(QStringLiteral("workTime_label"));
        workTime_label->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        workTime_label->setFrameShape(QFrame::Box);

        horizontalLayout_13->addWidget(workTime_label);


        verticalLayout_3->addLayout(horizontalLayout_13);


        gridLayout->addLayout(verticalLayout_3, 2, 2, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_11);

        label_15 = new QLabel(centralwidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_15->setFrameShape(QFrame::WinPanel);

        horizontalLayout_16->addWidget(label_15);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_12);


        verticalLayout_4->addLayout(horizontalLayout_16);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        label_17 = new QLabel(centralwidget);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_14->addWidget(label_17);

        startV_timeEdit = new QTimeEdit(centralwidget);
        startV_timeEdit->setObjectName(QStringLiteral("startV_timeEdit"));
        startV_timeEdit->setCursor(QCursor(Qt::PointingHandCursor));
        startV_timeEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        startV_timeEdit->setMaximumTime(QTime(23, 59, 59));
        startV_timeEdit->setMinimumTime(QTime(6, 0, 0));

        horizontalLayout_14->addWidget(startV_timeEdit);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_13);

        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_17->addWidget(label_9);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_14);


        horizontalLayout_14->addLayout(horizontalLayout_17);

        endV_timeEdit = new QTimeEdit(centralwidget);
        endV_timeEdit->setObjectName(QStringLiteral("endV_timeEdit"));
        endV_timeEdit->setCursor(QCursor(Qt::PointingHandCursor));
        endV_timeEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        endV_timeEdit->setMinimumTime(QTime(7, 0, 0));

        horizontalLayout_14->addWidget(endV_timeEdit);


        verticalLayout_4->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_9);

        add_btn = new QPushButton(centralwidget);
        add_btn->setObjectName(QStringLiteral("add_btn"));
        add_btn->setCursor(QCursor(Qt::PointingHandCursor));
        add_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout_15->addWidget(add_btn);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_10);


        verticalLayout_4->addLayout(horizontalLayout_15);


        gridLayout->addLayout(verticalLayout_4, 2, 0, 1, 1);

        line_3 = new QFrame(centralwidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_3, 2, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        ClientWindow->setCentralWidget(centralwidget);

        retranslateUi(ClientWindow);

        QMetaObject::connectSlotsByName(ClientWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ClientWindow)
    {
        ClientWindow->setWindowTitle(QApplication::translate("ClientWindow", "Aqua Table", 0));
        exit_btn->setText(QString());
        user_label->setText(QApplication::translate("ClientWindow", "Username", 0));
        label_2->setText(QApplication::translate("ClientWindow", "Name:", 0));
        name_label->setText(QString());
        label_3->setText(QApplication::translate("ClientWindow", "Surname:", 0));
        sur_label->setText(QString());
        label_5->setText(QApplication::translate("ClientWindow", "Phone:", 0));
        phone_label->setText(QString());
        edit_btn->setText(QApplication::translate("ClientWindow", "Edit", 0));
        history_btn->setText(QApplication::translate("ClientWindow", "History", 0));
        label_10->setText(QApplication::translate("ClientWindow", "Price", 0));
        label_11->setText(QApplication::translate("ClientWindow", "Adult:", 0));
        adult_label->setText(QString());
        label_12->setText(QApplication::translate("ClientWindow", "Child under 18:", 0));
        child18_label->setText(QString());
        label_13->setText(QApplication::translate("ClientWindow", "Child under 12:", 0));
        child12_label->setText(QString());
        err_label->setText(QString());
        label_7->setText(QApplication::translate("ClientWindow", "Booked clients:", 0));
        clientsnum_label->setText(QString());
        label_8->setText(QApplication::translate("ClientWindow", "Work time:", 0));
        workTime_label->setText(QString());
        label_15->setText(QApplication::translate("ClientWindow", "New time of visit", 0));
        label_17->setText(QApplication::translate("ClientWindow", "Time:", 0));
        label_9->setText(QApplication::translate("ClientWindow", "to", 0));
        add_btn->setText(QApplication::translate("ClientWindow", "Add", 0));
    } // retranslateUi

};

namespace Ui {
    class ClientWindow: public Ui_ClientWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTWINDOW_H
