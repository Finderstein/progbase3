/****************************************************************************
** Meta object code from reading C++ file 'regwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../AquaTable/regwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'regwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_RegWindow_t {
    QByteArrayData data[11];
    char stringdata0[173];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RegWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RegWindow_t qt_meta_stringdata_RegWindow = {
    {
QT_MOC_LITERAL(0, 0, 9), // "RegWindow"
QT_MOC_LITERAL(1, 10, 10), // "sendClient"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 7), // "Client*"
QT_MOC_LITERAL(4, 30, 18), // "on_reg_btn_clicked"
QT_MOC_LITERAL(5, 49, 19), // "on_exit_btn_clicked"
QT_MOC_LITERAL(6, 69, 25), // "on_usname_lin_textChanged"
QT_MOC_LITERAL(7, 95, 4), // "arg1"
QT_MOC_LITERAL(8, 100, 23), // "on_pas_line_textChanged"
QT_MOC_LITERAL(9, 124, 24), // "on_name_line_textChanged"
QT_MOC_LITERAL(10, 149, 23) // "on_sur_line_textChanged"

    },
    "RegWindow\0sendClient\0\0Client*\0"
    "on_reg_btn_clicked\0on_exit_btn_clicked\0"
    "on_usname_lin_textChanged\0arg1\0"
    "on_pas_line_textChanged\0"
    "on_name_line_textChanged\0"
    "on_sur_line_textChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RegWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   52,    2, 0x08 /* Private */,
       5,    0,   53,    2, 0x08 /* Private */,
       6,    1,   54,    2, 0x08 /* Private */,
       8,    1,   57,    2, 0x08 /* Private */,
       9,    1,   60,    2, 0x08 /* Private */,
      10,    1,   63,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    7,

       0        // eod
};

void RegWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RegWindow *_t = static_cast<RegWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendClient((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 1: _t->on_reg_btn_clicked(); break;
        case 2: _t->on_exit_btn_clicked(); break;
        case 3: _t->on_usname_lin_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->on_pas_line_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_name_line_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_sur_line_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Client* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (RegWindow::*_t)(Client * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RegWindow::sendClient)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject RegWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_RegWindow.data,
      qt_meta_data_RegWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *RegWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RegWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_RegWindow.stringdata0))
        return static_cast<void*>(const_cast< RegWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int RegWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void RegWindow::sendClient(Client * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
