/********************************************************************************
** Form generated from reading UI file 'regwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGWINDOW_H
#define UI_REGWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RegWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLineEdit *usname_lin;
    QLabel *label_3;
    QLineEdit *pas_line;
    QLabel *label_4;
    QLineEdit *name_line;
    QLabel *label_5;
    QLineEdit *sur_line;
    QLabel *label_6;
    QLineEdit *phone_line;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QLabel *label_8;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_9;
    QPushButton *reg_btn;
    QPushButton *exit_btn;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *fish_pix;

    void setupUi(QMainWindow *RegWindow)
    {
        if (RegWindow->objectName().isEmpty())
            RegWindow->setObjectName(QStringLiteral("RegWindow"));
        RegWindow->resize(378, 403);
        RegWindow->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        centralwidget = new QWidget(RegWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        usname_lin = new QLineEdit(centralwidget);
        usname_lin->setObjectName(QStringLiteral("usname_lin"));
        usname_lin->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(usname_lin);

        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        pas_line = new QLineEdit(centralwidget);
        pas_line->setObjectName(QStringLiteral("pas_line"));
        pas_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(pas_line);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout->addWidget(label_4);

        name_line = new QLineEdit(centralwidget);
        name_line->setObjectName(QStringLiteral("name_line"));
        name_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(name_line);

        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout->addWidget(label_5);

        sur_line = new QLineEdit(centralwidget);
        sur_line->setObjectName(QStringLiteral("sur_line"));
        sur_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(sur_line);

        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout->addWidget(label_6);

        phone_line = new QLineEdit(centralwidget);
        phone_line->setObjectName(QStringLiteral("phone_line"));
        phone_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        phone_line->setInputMethodHints(Qt::ImhDigitsOnly|Qt::ImhEmailCharactersOnly);
        phone_line->setMaxLength(12);

        verticalLayout->addWidget(phone_line);


        formLayout->setLayout(2, QFormLayout::LabelRole, verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 100, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_8->setFrameShape(QFrame::Box);

        verticalLayout_2->addWidget(label_8);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);


        formLayout->setLayout(2, QFormLayout::FieldRole, verticalLayout_2);

        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_9);

        reg_btn = new QPushButton(centralwidget);
        reg_btn->setObjectName(QStringLiteral("reg_btn"));
        reg_btn->setEnabled(false);
        reg_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(3, QFormLayout::FieldRole, reg_btn);

        exit_btn = new QPushButton(centralwidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(exit_btn->sizePolicy().hasHeightForWidth());
        exit_btn->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QStringLiteral("../icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        exit_btn->setIcon(icon);
        exit_btn->setIconSize(QSize(50, 35));
        exit_btn->setFlat(true);

        formLayout->setWidget(0, QFormLayout::LabelRole, exit_btn);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        fish_pix = new QLabel(centralwidget);
        fish_pix->setObjectName(QStringLiteral("fish_pix"));

        horizontalLayout->addWidget(fish_pix);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);

        RegWindow->setCentralWidget(centralwidget);

        retranslateUi(RegWindow);

        QMetaObject::connectSlotsByName(RegWindow);
    } // setupUi

    void retranslateUi(QMainWindow *RegWindow)
    {
        RegWindow->setWindowTitle(QApplication::translate("RegWindow", "Aqua Table", 0));
        label->setText(QApplication::translate("RegWindow", "Create new client account", 0));
        label_2->setText(QApplication::translate("RegWindow", "Username*", 0));
        label_3->setText(QApplication::translate("RegWindow", "Password*", 0));
        label_4->setText(QApplication::translate("RegWindow", "Name*", 0));
        label_5->setText(QApplication::translate("RegWindow", "Surname*", 0));
        label_6->setText(QApplication::translate("RegWindow", "Phone", 0));
        phone_line->setInputMask(QString());
        phone_line->setText(QString());
        label_8->setText(QApplication::translate("RegWindow", "<html><head/><body><p>Please, enter your real.</p><p>You wouldn`t be able to</p><p>change this fields.</p></body></html>", 0));
        label_9->setText(QApplication::translate("RegWindow", "*: Necessarily", 0));
        reg_btn->setText(QApplication::translate("RegWindow", "Register", 0));
        exit_btn->setText(QString());
        fish_pix->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class RegWindow: public Ui_RegWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGWINDOW_H
