/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *exit_btn;
    QPushButton *profile_btn;
    QPushButton *btn1;
    QPushButton *btn2;
    QPushButton *btn3;
    QLineEdit *search_line;
    QComboBox *search_comboBox;
    QPushButton *search_btn;
    QLabel *pix;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QPushButton *clients_btn;
    QPushButton *black_btn;
    QPushButton *workDays_btn;
    QPushButton *workers_btn;
    QPushButton *fired_btn;
    QSpacerItem *verticalSpacer;
    QTableWidget *tableWidget;
    QFrame *line;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(830, 527);
        MainWindow->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        exit_btn = new QPushButton(centralwidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(exit_btn->sizePolicy().hasHeightForWidth());
        exit_btn->setSizePolicy(sizePolicy);
        exit_btn->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon;
        icon.addFile(QStringLiteral("../icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        exit_btn->setIcon(icon);
        exit_btn->setIconSize(QSize(50, 35));
        exit_btn->setFlat(true);

        horizontalLayout->addWidget(exit_btn);

        profile_btn = new QPushButton(centralwidget);
        profile_btn->setObjectName(QStringLiteral("profile_btn"));
        profile_btn->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(profile_btn->sizePolicy().hasHeightForWidth());
        profile_btn->setSizePolicy(sizePolicy1);
        profile_btn->setCursor(QCursor(Qt::PointingHandCursor));
        profile_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout->addWidget(profile_btn);

        btn1 = new QPushButton(centralwidget);
        btn1->setObjectName(QStringLiteral("btn1"));
        btn1->setEnabled(false);
        sizePolicy1.setHeightForWidth(btn1->sizePolicy().hasHeightForWidth());
        btn1->setSizePolicy(sizePolicy1);
        btn1->setCursor(QCursor(Qt::PointingHandCursor));
        btn1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout->addWidget(btn1);

        btn2 = new QPushButton(centralwidget);
        btn2->setObjectName(QStringLiteral("btn2"));
        btn2->setEnabled(false);
        sizePolicy1.setHeightForWidth(btn2->sizePolicy().hasHeightForWidth());
        btn2->setSizePolicy(sizePolicy1);
        btn2->setCursor(QCursor(Qt::PointingHandCursor));
        btn2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout->addWidget(btn2);

        btn3 = new QPushButton(centralwidget);
        btn3->setObjectName(QStringLiteral("btn3"));
        btn3->setEnabled(false);
        sizePolicy1.setHeightForWidth(btn3->sizePolicy().hasHeightForWidth());
        btn3->setSizePolicy(sizePolicy1);
        btn3->setCursor(QCursor(Qt::PointingHandCursor));
        btn3->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout->addWidget(btn3);


        horizontalLayout_3->addLayout(horizontalLayout);

        search_line = new QLineEdit(centralwidget);
        search_line->setObjectName(QStringLiteral("search_line"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(search_line->sizePolicy().hasHeightForWidth());
        search_line->setSizePolicy(sizePolicy2);
        search_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout_3->addWidget(search_line);

        search_comboBox = new QComboBox(centralwidget);
        search_comboBox->setObjectName(QStringLiteral("search_comboBox"));
        search_comboBox->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(65, 65, 65);"));

        horizontalLayout_3->addWidget(search_comboBox);

        search_btn = new QPushButton(centralwidget);
        search_btn->setObjectName(QStringLiteral("search_btn"));
        search_btn->setCursor(QCursor(Qt::PointingHandCursor));
        search_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout_3->addWidget(search_btn);

        pix = new QLabel(centralwidget);
        pix->setObjectName(QStringLiteral("pix"));

        horizontalLayout_3->addWidget(pix);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        clients_btn = new QPushButton(centralwidget);
        clients_btn->setObjectName(QStringLiteral("clients_btn"));
        clients_btn->setCursor(QCursor(Qt::PointingHandCursor));
        clients_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(clients_btn);

        black_btn = new QPushButton(centralwidget);
        black_btn->setObjectName(QStringLiteral("black_btn"));
        black_btn->setCursor(QCursor(Qt::PointingHandCursor));
        black_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(black_btn);

        workDays_btn = new QPushButton(centralwidget);
        workDays_btn->setObjectName(QStringLiteral("workDays_btn"));
        workDays_btn->setCursor(QCursor(Qt::PointingHandCursor));
        workDays_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(workDays_btn);

        workers_btn = new QPushButton(centralwidget);
        workers_btn->setObjectName(QStringLiteral("workers_btn"));
        workers_btn->setCursor(QCursor(Qt::PointingHandCursor));
        workers_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(workers_btn);

        fired_btn = new QPushButton(centralwidget);
        fired_btn->setObjectName(QStringLiteral("fired_btn"));
        fired_btn->setCursor(QCursor(Qt::PointingHandCursor));
        fired_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(fired_btn);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout);

        tableWidget = new QTableWidget(centralwidget);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->viewport()->setProperty("cursor", QVariant(QCursor(Qt::PointingHandCursor)));
        tableWidget->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSortingEnabled(true);

        horizontalLayout_2->addWidget(tableWidget);


        gridLayout->addLayout(horizontalLayout_2, 2, 0, 1, 1);

        line = new QFrame(centralwidget);
        line->setObjectName(QStringLiteral("line"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(line->sizePolicy().hasHeightForWidth());
        line->setSizePolicy(sizePolicy3);
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 1, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        line->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Aqua Table", 0));
        exit_btn->setText(QString());
        profile_btn->setText(QApplication::translate("MainWindow", "Profile", 0));
        btn1->setText(QApplication::translate("MainWindow", "Add to \n"
"black list", 0));
        btn2->setText(QApplication::translate("MainWindow", "History", 0));
        btn3->setText(QApplication::translate("MainWindow", "Set \n"
"visited", 0));
        search_line->setInputMask(QString());
        search_line->setText(QString());
        search_line->setPlaceholderText(QApplication::translate("MainWindow", "Search", 0));
        search_btn->setText(QApplication::translate("MainWindow", "Search", 0));
        pix->setText(QString());
        clients_btn->setText(QApplication::translate("MainWindow", "Clients", 0));
        black_btn->setText(QApplication::translate("MainWindow", "Black list", 0));
        workDays_btn->setText(QApplication::translate("MainWindow", "Work days", 0));
        workers_btn->setText(QApplication::translate("MainWindow", "Personal", 0));
        fired_btn->setText(QApplication::translate("MainWindow", "Fired", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
