/********************************************************************************
** Form generated from reading UI file 'clietnhistorydialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIETNHISTORYDIALOG_H
#define UI_CLIETNHISTORYDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_ClietnHistoryDialog
{
public:
    QGridLayout *gridLayout;
    QPushButton *exit_btn;
    QPushButton *rem_btn;
    QSpacerItem *horizontalSpacer;
    QTableWidget *history_table;
    QLabel *err_label;

    void setupUi(QDialog *ClietnHistoryDialog)
    {
        if (ClietnHistoryDialog->objectName().isEmpty())
            ClietnHistoryDialog->setObjectName(QStringLiteral("ClietnHistoryDialog"));
        ClietnHistoryDialog->resize(720, 363);
        ClietnHistoryDialog->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        gridLayout = new QGridLayout(ClietnHistoryDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        exit_btn = new QPushButton(ClietnHistoryDialog);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(exit_btn->sizePolicy().hasHeightForWidth());
        exit_btn->setSizePolicy(sizePolicy);
        exit_btn->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon;
        icon.addFile(QStringLiteral("../icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        exit_btn->setIcon(icon);
        exit_btn->setIconSize(QSize(50, 35));
        exit_btn->setFlat(true);

        gridLayout->addWidget(exit_btn, 0, 0, 1, 1);

        rem_btn = new QPushButton(ClietnHistoryDialog);
        rem_btn->setObjectName(QStringLiteral("rem_btn"));
        rem_btn->setEnabled(false);
        rem_btn->setCursor(QCursor(Qt::PointingHandCursor));
        rem_btn->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(rem_btn, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(530, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);

        history_table = new QTableWidget(ClietnHistoryDialog);
        if (history_table->columnCount() < 7)
            history_table->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        history_table->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        history_table->setObjectName(QStringLiteral("history_table"));
        history_table->viewport()->setProperty("cursor", QVariant(QCursor(Qt::PointingHandCursor)));
        history_table->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        history_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        history_table->setSortingEnabled(true);

        gridLayout->addWidget(history_table, 1, 0, 1, 3);

        err_label = new QLabel(ClietnHistoryDialog);
        err_label->setObjectName(QStringLiteral("err_label"));
        err_label->setStyleSheet(QStringLiteral("color: rgb(170, 0, 0);"));

        gridLayout->addWidget(err_label, 2, 0, 1, 3);


        retranslateUi(ClietnHistoryDialog);

        QMetaObject::connectSlotsByName(ClietnHistoryDialog);
    } // setupUi

    void retranslateUi(QDialog *ClietnHistoryDialog)
    {
        ClietnHistoryDialog->setWindowTitle(QApplication::translate("ClietnHistoryDialog", "Aqua Table", 0));
        exit_btn->setText(QString());
        rem_btn->setText(QApplication::translate("ClietnHistoryDialog", "Remove", 0));
        QTableWidgetItem *___qtablewidgetitem = history_table->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ClietnHistoryDialog", "Username", 0));
        QTableWidgetItem *___qtablewidgetitem1 = history_table->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ClietnHistoryDialog", "Name", 0));
        QTableWidgetItem *___qtablewidgetitem2 = history_table->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("ClietnHistoryDialog", "Surname", 0));
        QTableWidgetItem *___qtablewidgetitem3 = history_table->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("ClietnHistoryDialog", "Phone", 0));
        QTableWidgetItem *___qtablewidgetitem4 = history_table->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("ClietnHistoryDialog", "Date of visit", 0));
        QTableWidgetItem *___qtablewidgetitem5 = history_table->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("ClietnHistoryDialog", "Start time visit", 0));
        QTableWidgetItem *___qtablewidgetitem6 = history_table->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("ClietnHistoryDialog", "End time visit", 0));
        err_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ClietnHistoryDialog: public Ui_ClietnHistoryDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIETNHISTORYDIALOG_H
