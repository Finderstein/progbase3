/********************************************************************************
** Form generated from reading UI file 'mainadminprofdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINADMINPROFDIALOG_H
#define UI_MAINADMINPROFDIALOG_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_MainAdminProfDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFormLayout *formLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLineEdit *pas_line;
    QLineEdit *name_line;
    QLineEdit *sur_line;
    QLineEdit *phone_line;
    QDateEdit *birth_dateEdit;
    QComboBox *ed_box;
    QComboBox *sex_box;
    QLineEdit *user_line;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *pos_line;
    QLabel *label_12;
    QComboBox *access_comboBox;
    QLabel *label_13;
    QDateEdit *empl_date;
    QDoubleSpinBox *salary_line;
    QLabel *label_14;
    QLineEdit *workdays_line;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *MainAdminProfDialog)
    {
        if (MainAdminProfDialog->objectName().isEmpty())
            MainAdminProfDialog->setObjectName(QStringLiteral("MainAdminProfDialog"));
        MainAdminProfDialog->resize(391, 508);
        MainAdminProfDialog->setStyleSheet(QLatin1String("background-color: rgb(65, 65, 255);\n"
"background-color: rgb(155, 222, 255);"));
        gridLayout = new QGridLayout(MainAdminProfDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(MainAdminProfDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setFrameShape(QFrame::WinPanel);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_2 = new QLabel(MainAdminProfDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(MainAdminProfDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(MainAdminProfDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(MainAdminProfDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(MainAdminProfDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_6);

        label_7 = new QLabel(MainAdminProfDialog);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_7);

        label_8 = new QLabel(MainAdminProfDialog);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout->setWidget(7, QFormLayout::LabelRole, label_8);

        label_9 = new QLabel(MainAdminProfDialog);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_9);

        pas_line = new QLineEdit(MainAdminProfDialog);
        pas_line->setObjectName(QStringLiteral("pas_line"));
        pas_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(1, QFormLayout::FieldRole, pas_line);

        name_line = new QLineEdit(MainAdminProfDialog);
        name_line->setObjectName(QStringLiteral("name_line"));
        name_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(2, QFormLayout::FieldRole, name_line);

        sur_line = new QLineEdit(MainAdminProfDialog);
        sur_line->setObjectName(QStringLiteral("sur_line"));
        sur_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(3, QFormLayout::FieldRole, sur_line);

        phone_line = new QLineEdit(MainAdminProfDialog);
        phone_line->setObjectName(QStringLiteral("phone_line"));
        phone_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        phone_line->setMaxLength(12);

        formLayout->setWidget(8, QFormLayout::FieldRole, phone_line);

        birth_dateEdit = new QDateEdit(MainAdminProfDialog);
        birth_dateEdit->setObjectName(QStringLiteral("birth_dateEdit"));
        birth_dateEdit->setCursor(QCursor(Qt::PointingHandCursor));
        birth_dateEdit->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(65, 65, 65);"));
        birth_dateEdit->setMaximumDate(QDate(7777, 11, 1));
        birth_dateEdit->setCalendarPopup(true);
        birth_dateEdit->setDate(QDate(1980, 1, 1));

        formLayout->setWidget(4, QFormLayout::FieldRole, birth_dateEdit);

        ed_box = new QComboBox(MainAdminProfDialog);
        ed_box->setObjectName(QStringLiteral("ed_box"));
        ed_box->setCursor(QCursor(Qt::PointingHandCursor));
        ed_box->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);"));

        formLayout->setWidget(6, QFormLayout::FieldRole, ed_box);

        sex_box = new QComboBox(MainAdminProfDialog);
        sex_box->setObjectName(QStringLiteral("sex_box"));
        sex_box->setCursor(QCursor(Qt::PointingHandCursor));
        sex_box->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);"));

        formLayout->setWidget(7, QFormLayout::FieldRole, sex_box);

        user_line = new QLineEdit(MainAdminProfDialog);
        user_line->setObjectName(QStringLiteral("user_line"));
        user_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(0, QFormLayout::FieldRole, user_line);

        label_10 = new QLabel(MainAdminProfDialog);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_10);

        label_11 = new QLabel(MainAdminProfDialog);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout->setWidget(10, QFormLayout::LabelRole, label_11);

        pos_line = new QLineEdit(MainAdminProfDialog);
        pos_line->setObjectName(QStringLiteral("pos_line"));
        pos_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(10, QFormLayout::FieldRole, pos_line);

        label_12 = new QLabel(MainAdminProfDialog);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout->setWidget(11, QFormLayout::LabelRole, label_12);

        access_comboBox = new QComboBox(MainAdminProfDialog);
        access_comboBox->setObjectName(QStringLiteral("access_comboBox"));
        access_comboBox->setCursor(QCursor(Qt::PointingHandCursor));
        access_comboBox->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(63, 63, 63);"));

        formLayout->setWidget(11, QFormLayout::FieldRole, access_comboBox);

        label_13 = new QLabel(MainAdminProfDialog);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_13);

        empl_date = new QDateEdit(MainAdminProfDialog);
        empl_date->setObjectName(QStringLiteral("empl_date"));
        empl_date->setCursor(QCursor(Qt::PointingHandCursor));
        empl_date->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(65, 65, 65);"));
        empl_date->setMinimumDate(QDate(1980, 9, 1));
        empl_date->setCalendarPopup(true);

        formLayout->setWidget(5, QFormLayout::FieldRole, empl_date);

        salary_line = new QDoubleSpinBox(MainAdminProfDialog);
        salary_line->setObjectName(QStringLiteral("salary_line"));
        salary_line->setCursor(QCursor(Qt::PointingHandCursor));
        salary_line->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(65, 65, 65);"));
        salary_line->setMaximum(1e+07);

        formLayout->setWidget(9, QFormLayout::FieldRole, salary_line);

        label_14 = new QLabel(MainAdminProfDialog);
        label_14->setObjectName(QStringLiteral("label_14"));

        formLayout->setWidget(12, QFormLayout::LabelRole, label_14);

        workdays_line = new QLineEdit(MainAdminProfDialog);
        workdays_line->setObjectName(QStringLiteral("workdays_line"));
        workdays_line->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        formLayout->setWidget(12, QFormLayout::FieldRole, workdays_line);


        verticalLayout->addLayout(formLayout);

        buttonBox = new QDialogButtonBox(MainAdminProfDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setCursor(QCursor(Qt::PointingHandCursor));
        buttonBox->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Save);

        verticalLayout->addWidget(buttonBox);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(MainAdminProfDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), MainAdminProfDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), MainAdminProfDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(MainAdminProfDialog);
    } // setupUi

    void retranslateUi(QDialog *MainAdminProfDialog)
    {
        MainAdminProfDialog->setWindowTitle(QApplication::translate("MainAdminProfDialog", "Aqua Table", 0));
        label->setText(QApplication::translate("MainAdminProfDialog", "Your profile", 0));
        label_2->setText(QApplication::translate("MainAdminProfDialog", "Username:", 0));
        label_3->setText(QApplication::translate("MainAdminProfDialog", "Password:", 0));
        label_4->setText(QApplication::translate("MainAdminProfDialog", "Name:", 0));
        label_5->setText(QApplication::translate("MainAdminProfDialog", "Surname:", 0));
        label_6->setText(QApplication::translate("MainAdminProfDialog", "Date of birth:", 0));
        label_7->setText(QApplication::translate("MainAdminProfDialog", "Education:", 0));
        label_8->setText(QApplication::translate("MainAdminProfDialog", "Sex:", 0));
        label_9->setText(QApplication::translate("MainAdminProfDialog", "Phone:", 0));
        phone_line->setPlaceholderText(QApplication::translate("MainAdminProfDialog", "123456789012", 0));
        birth_dateEdit->setDisplayFormat(QApplication::translate("MainAdminProfDialog", "dd.MM.yyyy", 0));
        ed_box->clear();
        ed_box->insertItems(0, QStringList()
         << QApplication::translate("MainAdminProfDialog", "High", 0)
         << QApplication::translate("MainAdminProfDialog", "Middle", 0)
         << QApplication::translate("MainAdminProfDialog", "Low", 0)
         << QApplication::translate("MainAdminProfDialog", "Uneducated", 0)
        );
        sex_box->clear();
        sex_box->insertItems(0, QStringList()
         << QApplication::translate("MainAdminProfDialog", "Man", 0)
         << QApplication::translate("MainAdminProfDialog", "Woman", 0)
         << QApplication::translate("MainAdminProfDialog", "Other", 0)
        );
        label_10->setText(QApplication::translate("MainAdminProfDialog", "Salary:", 0));
        label_11->setText(QApplication::translate("MainAdminProfDialog", "Position:", 0));
        label_12->setText(QApplication::translate("MainAdminProfDialog", "Access:", 0));
        access_comboBox->clear();
        access_comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainAdminProfDialog", "Fired", 0)
         << QApplication::translate("MainAdminProfDialog", "Normal", 0)
         << QApplication::translate("MainAdminProfDialog", "Admin", 0)
         << QApplication::translate("MainAdminProfDialog", "None", 0)
        );
        label_13->setText(QApplication::translate("MainAdminProfDialog", "Date of empl:", 0));
        empl_date->setDisplayFormat(QApplication::translate("MainAdminProfDialog", "dd.MM.yyyy", 0));
        label_14->setText(QApplication::translate("MainAdminProfDialog", "Workdays:", 0));
        workdays_line->setInputMask(QString());
        workdays_line->setPlaceholderText(QApplication::translate("MainAdminProfDialog", "Monday, ...", 0));
    } // retranslateUi

};

namespace Ui {
    class MainAdminProfDialog: public Ui_MainAdminProfDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINADMINPROFDIALOG_H
