/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../AquaTable/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[28];
    char stringdata0[391];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "sendClient"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 7), // "Client*"
QT_MOC_LITERAL(4, 31, 10), // "sendStatus"
QT_MOC_LITERAL(5, 42, 2), // "st"
QT_MOC_LITERAL(6, 45, 10), // "sendWorker"
QT_MOC_LITERAL(7, 56, 7), // "Worker*"
QT_MOC_LITERAL(8, 64, 11), // "sendWorkday"
QT_MOC_LITERAL(9, 76, 9), // "WorkDays*"
QT_MOC_LITERAL(10, 86, 20), // "on_black_btn_clicked"
QT_MOC_LITERAL(11, 107, 22), // "on_clients_btn_clicked"
QT_MOC_LITERAL(12, 130, 23), // "on_workDays_btn_clicked"
QT_MOC_LITERAL(13, 154, 22), // "on_profile_btn_clicked"
QT_MOC_LITERAL(14, 177, 19), // "on_exit_btn_clicked"
QT_MOC_LITERAL(15, 197, 22), // "on_workers_btn_clicked"
QT_MOC_LITERAL(16, 220, 15), // "on_btn1_clicked"
QT_MOC_LITERAL(17, 236, 15), // "on_btn2_clicked"
QT_MOC_LITERAL(18, 252, 15), // "on_btn3_clicked"
QT_MOC_LITERAL(19, 268, 22), // "on_tableWidget_clicked"
QT_MOC_LITERAL(20, 291, 5), // "index"
QT_MOC_LITERAL(21, 297, 20), // "on_fired_btn_clicked"
QT_MOC_LITERAL(22, 318, 21), // "on_search_btn_clicked"
QT_MOC_LITERAL(23, 340, 9), // "getStatus"
QT_MOC_LITERAL(24, 350, 4), // "stat"
QT_MOC_LITERAL(25, 355, 9), // "getWorker"
QT_MOC_LITERAL(26, 365, 14), // "editUserWorker"
QT_MOC_LITERAL(27, 380, 10) // "editWorker"

    },
    "MainWindow\0sendClient\0\0Client*\0"
    "sendStatus\0st\0sendWorker\0Worker*\0"
    "sendWorkday\0WorkDays*\0on_black_btn_clicked\0"
    "on_clients_btn_clicked\0on_workDays_btn_clicked\0"
    "on_profile_btn_clicked\0on_exit_btn_clicked\0"
    "on_workers_btn_clicked\0on_btn1_clicked\0"
    "on_btn2_clicked\0on_btn3_clicked\0"
    "on_tableWidget_clicked\0index\0"
    "on_fired_btn_clicked\0on_search_btn_clicked\0"
    "getStatus\0stat\0getWorker\0editUserWorker\0"
    "editWorker"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  114,    2, 0x06 /* Public */,
       4,    1,  117,    2, 0x06 /* Public */,
       6,    1,  120,    2, 0x06 /* Public */,
       8,    1,  123,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,  126,    2, 0x08 /* Private */,
      11,    0,  127,    2, 0x08 /* Private */,
      12,    0,  128,    2, 0x08 /* Private */,
      13,    0,  129,    2, 0x08 /* Private */,
      14,    0,  130,    2, 0x08 /* Private */,
      15,    0,  131,    2, 0x08 /* Private */,
      16,    0,  132,    2, 0x08 /* Private */,
      17,    0,  133,    2, 0x08 /* Private */,
      18,    0,  134,    2, 0x08 /* Private */,
      19,    1,  135,    2, 0x08 /* Private */,
      21,    0,  138,    2, 0x08 /* Private */,
      22,    0,  139,    2, 0x08 /* Private */,
      23,    1,  140,    2, 0x0a /* Public */,
      25,    1,  143,    2, 0x0a /* Public */,
      26,    1,  146,    2, 0x0a /* Public */,
      27,    1,  149,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void, 0x80000000 | 9,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void, 0x80000000 | 7,    2,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendClient((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 1: _t->sendStatus((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->sendWorker((*reinterpret_cast< Worker*(*)>(_a[1]))); break;
        case 3: _t->sendWorkday((*reinterpret_cast< WorkDays*(*)>(_a[1]))); break;
        case 4: _t->on_black_btn_clicked(); break;
        case 5: _t->on_clients_btn_clicked(); break;
        case 6: _t->on_workDays_btn_clicked(); break;
        case 7: _t->on_profile_btn_clicked(); break;
        case 8: _t->on_exit_btn_clicked(); break;
        case 9: _t->on_workers_btn_clicked(); break;
        case 10: _t->on_btn1_clicked(); break;
        case 11: _t->on_btn2_clicked(); break;
        case 12: _t->on_btn3_clicked(); break;
        case 13: _t->on_tableWidget_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 14: _t->on_fired_btn_clicked(); break;
        case 15: _t->on_search_btn_clicked(); break;
        case 16: _t->getStatus((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->getWorker((*reinterpret_cast< Worker*(*)>(_a[1]))); break;
        case 18: _t->editUserWorker((*reinterpret_cast< Worker*(*)>(_a[1]))); break;
        case 19: _t->editWorker((*reinterpret_cast< Worker*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Client* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Worker* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< WorkDays* >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Worker* >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Worker* >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Worker* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(Client * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendClient)) {
                *result = 0;
            }
        }
        {
            typedef void (MainWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendStatus)) {
                *result = 1;
            }
        }
        {
            typedef void (MainWindow::*_t)(Worker * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendWorker)) {
                *result = 2;
            }
        }
        {
            typedef void (MainWindow::*_t)(WorkDays * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendWorkday)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::sendClient(Client * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::sendStatus(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::sendWorker(Worker * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::sendWorkday(WorkDays * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
