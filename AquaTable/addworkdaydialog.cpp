#include "addworkdaydialog.h"
#include "ui_addworkdaydialog.h"
#include <QMessageBox>

AddWorkdayDialog::AddWorkdayDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddWorkdayDialog)
{
    ui->setupUi(this);

    this->status = ADD_ST;
}

AddWorkdayDialog::~AddWorkdayDialog()
{
    delete ui;
}

void AddWorkdayDialog::on_buttonBox_accepted()
{
    QSqlQuery query;
    if(this->status == ADD_ST)
    {
        query.prepare("INSERT INTO WorkDays (Date, StartTime, EndTime, Price_adult, Price_under18, Price_under12) VALUES (:date, :open, :close, :adult, :under18, :under12)");
        query.bindValue(":date", ui->date->text());
        query.bindValue(":open",ui->open_timeEdit->text());
        query.bindValue(":close",ui->close_timeEdit->text());
        query.bindValue(":adult",ui->adult_spinBox->value());
        query.bindValue(":under18",ui->under18_spinBox->value());
        query.bindValue(":under12",ui->under12_spinBox->value());
        if(!query.exec())
        {
            QMessageBox::information(this, "Error new day", "Can`t create new day");
        }
    }
    else
    {
        query.prepare("UPDATE Workdays SET StartTime = :open, EndTime = :close, Price_adult = :adult, Price_under18 = :under18, Price_under12 = :under12 Where Date = :date");
        query.bindValue(":date", ui->date->text());
        query.bindValue(":open", ui->open_timeEdit->text());
        query.bindValue(":close", ui->close_timeEdit->text());
        query.bindValue(":adult", ui->adult_spinBox->value());
        query.bindValue(":under18", ui->under18_spinBox->value());
        query.bindValue(":under12", ui->under12_spinBox->value());
        query.exec();
    }

}

void AddWorkdayDialog::getWorkday(WorkDays *day)
{
    this->status = EDIT_ST;
    ui->label->setText("Edit workday");
    ui->date->setDate(QDate::fromString(day->getDate(), "dd.MM.yyyy"));
    ui->open_timeEdit->setTime(QTime::fromString(day->getOpen(), "HH:mm"));
    ui->close_timeEdit->setTime(QTime::fromString(day->getClose(), "HH:mm"));
    ui->adult_spinBox->setValue(day->getAdult());
    ui->under18_spinBox->setValue(day->getUnder18());
    ui->under12_spinBox->setValue(day->getUnder12());
    ui->date->setEnabled(false);
}
