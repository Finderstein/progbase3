#ifndef MAINADMINPROFDIALOG_H
#define MAINADMINPROFDIALOG_H

#include <QDialog>
#include "databasemanager.h"

enum Status {
    EDIT = 0,
    ADD
};

namespace Ui {
class MainAdminProfDialog;
}

class MainAdminProfDialog : public QDialog
{
    Q_OBJECT
    Worker *worker;
    int status;
public:
    explicit MainAdminProfDialog(QWidget *parent = 0);
    ~MainAdminProfDialog();

private:
    Ui::MainAdminProfDialog *ui;
    DatabaseManager *_db;
public slots:
    void getWorker(Worker *);
    void getStatus(int st);
signals:
    void returnWorker(Worker *);
private slots:
    void on_buttonBox_accepted();
};

#endif // MAINADMINPROFDIALOG_H
