#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>

enum ClientStatus {
    BLACK_ST = 0,
    ACTIVE_ST,
    VISIT_ACTIVE_ST,
    VISIT_UNACTIVE_ST
};

class Client : public QObject
{
    Q_OBJECT
    QString username;
    QString password;
    QString name;
    QString surname;
    QString phone;
    QString dateOfVisit;
    QString startTimeVisit;
    QString endTimeVisit;
    int id;
    int active;

public:
    explicit Client(QObject *parent = 0);

    void setUsername(QString username);
    void setPassword(QString password);
    void setName(QString name);
    void setSurname(QString surname);
    void setPhone(QString phone);
    void setDate(QString date);
    void setStartTimeVisit(QString name);
    void setEndTimeVisit(QString name);
    void setId(int id);
    void setActive(int act);

    QString getUsername();
    QString getPassword();
    QString getName();
    QString getSurname();
    QString getPhone();
    QString getDate();
    QString getStartTimeVisit();
    QString getEndTimeVisit();
    int getId();
    int getActive();

signals:

public slots:
};

#endif // CLIENT_H
