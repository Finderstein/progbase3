#include "workdays.h"

WorkDays::WorkDays(QObject *parent) : QObject(parent)
{

}

void WorkDays::setDate(QString date)
{
    this->date = date;
}

void WorkDays::setOpen(QString open)
{
    this->open = open;
}

void WorkDays::setClose(QString close)
{
    this->close = close;
}

void WorkDays::setClNum(int num)
{
    this->clientsNum = num;
}

void WorkDays::setAdult(int price)
{
    this->priceAdult = price;
}

void WorkDays::setUnder18(int price)
{
    this->priceUnder18 = price;
}

void WorkDays::setUnder12(int price)
{
    this->priceUnder12 = price;
}



QString WorkDays::getDate()
{
    return this->date;
}

QString WorkDays::getOpen()
{
    return this->open;
}

QString WorkDays::getClose()
{
    return this->close;
}

int WorkDays::getClNum()
{
    return this->clientsNum;
}

int WorkDays::getAdult()
{
    return this->priceAdult;
}

int WorkDays::getUnder18()
{
    return this->priceUnder18;
}

int WorkDays::getUnder12()
{
    return this->priceUnder12;
}

