#ifndef ADDWORKDAYDIALOG_H
#define ADDWORKDAYDIALOG_H

#include <QDialog>
#include <databasemanager.h>

enum AddDay {
    ADD_ST,
    EDIT_ST
};

namespace Ui {
class AddWorkdayDialog;
}

class AddWorkdayDialog : public QDialog
{
    Q_OBJECT
    int status;

public:
    explicit AddWorkdayDialog(QWidget *parent = 0);
    ~AddWorkdayDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::AddWorkdayDialog *ui;
    DatabaseManager *_db;
public slots:
    void getWorkday(WorkDays *);
};

#endif // ADDWORKDAYDIALOG_H
