#include "loginwindow.h"
#include "logorregwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LogOrRegWindow w;
    w.show();

    return a.exec();
}
