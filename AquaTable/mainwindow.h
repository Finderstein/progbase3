#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <databasemanager.h>
#include "mainadminprofdialog.h"
#include "loginwindow.h"

enum TableStatus
{
    CLIENTS_TABLE = 0,
    BLACK_TABLE,
    DAYS_TABLE,
    WORKERS_TABLE,
    FIRED_TABLE
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    int userStatus;
    int tableStatus;
    Worker *userWorker;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void clientsVisitTable(QString search, int table);
    void blackClientsTable(QString search, int table);
    void workDaysTable(QString search, int table);
    void workersTable(int stat, QString search, int table);


private slots:
    void on_black_btn_clicked();

    void on_clients_btn_clicked();

    void on_workDays_btn_clicked();

    void on_profile_btn_clicked();

    void on_exit_btn_clicked();

    void on_workers_btn_clicked();

    void on_btn1_clicked();

    void on_btn2_clicked();

    void on_btn3_clicked();

    void on_tableWidget_clicked(const QModelIndex &index);

    void on_fired_btn_clicked();

    void on_search_btn_clicked();

private:
    Ui::MainWindow *ui;
    DatabaseManager *_db;
public slots:
    void getStatus(int stat);
    void getWorker(Worker *);
    void editUserWorker(Worker *);
    void editWorker(Worker *);
signals:
    void sendClient(Client *);
    void sendStatus(int st);
    void sendWorker(Worker *);
    void sendWorkday(WorkDays *);
};

#endif // MAINWINDOW_H
