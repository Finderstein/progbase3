#include "clientwindow.h"
#include "ui_clientwindow.h"
#include <QDebug>
#include <QCryptographicHash>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <loginwindow.h>
#include <clienteditdialog.h>
#include "clietnhistorydialog.h"

ClientWindow::ClientWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClientWindow)
{
    ui->setupUi(this);

    QString path = "../AquaTable/login.sqlite3";
    _db = new DatabaseManager(path);
    if(!_db->open()) {
        qDebug() << "can't connect: " ;
        return;
    }

    on_calendarWidget_clicked(ui->calendarWidget->selectedDate());
}

ClientWindow::~ClientWindow()
{
    _db->close();
    delete _db;
    delete ui;
}

void ClientWindow::getClient(Client *c)
{
    this->client = c;
    ui->user_label->setText(client->getUsername());
    ui->name_label->setText(client->getName());
    ui->sur_label->setText(client->getSurname());
    ui->phone_label->setText(client->getPhone());
}

void ClientWindow::editClient(Client *c)
{
    if (!_db->editClient(c, this->client->getUsername()) || !_db->editClientVisits(c)) {
        QMessageBox::information(this, "Edit error", "Can`t change your account");
        return;
    }

    this->client = c;
    ui->user_label->setText(client->getUsername());
    ui->name_label->setText(client->getName());
    ui->sur_label->setText(client->getSurname());
    ui->phone_label->setText(client->getPhone());
}



void ClientWindow::on_edit_btn_clicked()
{
    ClientEditDialog dialog;
    connect(this, SIGNAL(sendClient(Client*)), &dialog, SLOT(getClient(Client*)));
    connect(&dialog, SIGNAL(returnClient(Client*)), this, SLOT(editClient(Client*)));
    Client *c = new Client();
    c->setUsername(client->getUsername());
    c->setPassword(client->getPassword());
    c->setName(client->getName());
    c->setSurname(client->getSurname());
    c->setDate(client->getDate());
    c->setStartTimeVisit(client->getStartTimeVisit());
    c->setEndTimeVisit(client->getEndTimeVisit());
    c->setPhone(client->getPhone());
    c->setId(client->getId());
    c->setActive(client->getActive());
    emit sendClient(c);
    dialog.exec();
}

void ClientWindow::on_exit_btn_clicked()
{
    LogInWindow *w = new LogInWindow();
    close();

    w->show();
}

static QString GetRandomString()
{
   const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
   const int randomStringLength = 4; // assuming you want random strings of 12 characters

   QString randomString;
   for(int i=0; i<randomStringLength; ++i)
   {
       int index = qrand() % possibleCharacters.length();
       QChar nextChar = possibleCharacters.at(index);
       randomString.append(nextChar);
   }
   return randomString;
}

void ClientWindow::on_add_btn_clicked()
{
    if(!_db->dayExists(ui->calendarWidget->selectedDate().toString("dd.MM.yyyy")))
    {
        QMessageBox::information(this, "Failed", "Aquapark has no shedule for this day!");
        return;
    }
    if(ui->endV_timeEdit->dateTime().operator <=(ui->startV_timeEdit->dateTime()))
    {
        QMessageBox::information(this, "Failed", "Invalid duration for visit!");
        return;
    }
    QSqlQuery query;
    //QString str = ui->usname_lin->text();
    query.prepare("INSERT INTO Clients (Username, Password, Name, Surname, Date_of_visit, Start_time_visit, End_time_visit, Phone, Active) VALUES (:login, :pas, :name, :surname, :date, :start, :end, :phone, :active)");
    query.bindValue(":login", client->getUsername());
    query.bindValue(":pas", client->getPassword());
    query.bindValue(":name", client->getName());
    query.bindValue(":surname", client->getSurname());
    query.bindValue(":date", ui->calendarWidget->selectedDate().toString("dd.MM.yyyy"));
    query.bindValue(":start", ui->startV_timeEdit->text());
    query.bindValue(":end", ui->endV_timeEdit->text());
    query.bindValue(":phone", client->getPhone());
    query.bindValue(":active", VISIT_ACTIVE_ST);
    if (!query.exec()) {
        on_add_btn_clicked();
        return;
    }
    else
    {
        query.prepare("UPDATE WorkDays SET ClientsNum = ClientsNum + 1 Where Date = :date");
        query.bindValue(":date",ui->calendarWidget->selectedDate().toString("dd.MM.yyyy"));
        query.exec();
        QMessageBox::information(this, "Successful", "Your time have been added");
        on_calendarWidget_clicked(ui->calendarWidget->selectedDate());
    }

}

void ClientWindow::on_calendarWidget_clicked(const QDate &date)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM WorkDays WHERE Date = :date");
    query.bindValue(":date", ui->calendarWidget->selectedDate().toString("dd.MM.yyyy"));
    query.exec();
    if(query.next())
    {
        ui->workTime_label->setText(query.value("StartTime").toString() + "-" + query.value("EndTime").toString());
        ui->clientsnum_label->setText(QString::number(query.value("ClientsNum").toInt()));
        ui->adult_label->setText(QString::number(query.value("Price_adult").toInt()));
        ui->child18_label->setText(QString::number(query.value("Price_under18").toInt()));
        ui->child12_label->setText(QString::number(query.value("Price_under12").toInt()));
        ui->err_label->setText("");
    }
    else
    {
        ui->workTime_label->setText("");
        ui->clientsnum_label->setText("");
        ui->adult_label->setText("");
        ui->child18_label->setText("");
        ui->child12_label->setText("");
        ui->err_label->setText("Aquapark has no shedule for this day");
    }
}

void ClientWindow::on_history_btn_clicked()
{
    ClietnHistoryDialog dialog;
    connect(this, SIGNAL(sendClient(Client*)), &dialog, SLOT(getClient(Client*)));
    emit sendClient(client);
    dialog.exec();
}
