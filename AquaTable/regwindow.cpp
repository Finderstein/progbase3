#include "regwindow.h"
#include "ui_regwindow.h"
#include "mainwindow.h"
#include "clientwindow.h"
#include <QDebug>
#include <QCryptographicHash>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

RegWindow::RegWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RegWindow)
{
    ui->setupUi(this);
    QPixmap pix("../orfish_smal.jpg");
    ui->fish_pix->setPixmap(pix);

    QString path = "../AquaTable/login.sqlite3";
    _db = new DatabaseManager(path);
    if(!_db->open()) {
        qDebug() << "can't connect: " ;
        return;

    }

    ui->phone_line->setValidator(new QRegExpValidator(QRegExp("[0-9]\\d{12,12}")));
}

RegWindow::~RegWindow()
{
    delete ui;
}

void RegWindow::on_reg_btn_clicked()
{
    if(_db->accExists(ui->usname_lin->text()))
    {
        QMessageBox::information(this, "Failed", "User with such username already exists");
        return;
    }
    QSqlQuery query;
    //QString str = ui->usname_lin->text();
    query.prepare("INSERT INTO Clients (Username, Password, Name, Surname, Date_of_visit, Start_time_visit, End_time_visit, Phone, Active) VALUES (:login, :pas, :name, :surname, :date, :start, :end, :phone, :active)");
    query.bindValue(":login", ui->usname_lin->text());
    QString pas = ui->pas_line->text() + "@@14f#_!";
    QByteArray hashArray = QCryptographicHash::hash(pas.toUtf8(), QCryptographicHash::Md5);
    QString passwordHash = QString(hashArray.toHex());
    query.bindValue(":pas", passwordHash);
    query.bindValue(":name", ui->name_line->text());
    query.bindValue(":surname", ui->sur_line->text());
    query.bindValue(":date", "");
    query.bindValue(":start", "");
    query.bindValue(":end", "");
    query.bindValue(":phone", ui->phone_line->text());
    query.bindValue(":active", ACTIVE_ST);
    if (!query.exec()) {
        QMessageBox::information(this, "Reg error", query.lastError().text());
        return;
    }

    QList<Client *> users = _db->selectAllClients();
    for(auto u : users)
    {
        if(passwordHash == u->getPassword() && ui->usname_lin->text() == u->getUsername())
        {
            close();

            ClientWindow *c = new ClientWindow();
            connect(this, SIGNAL(sendClient(Client *)), c, SLOT(getClient(Client *)));
            emit sendClient(u);
            c->show();

        }
    }


}

void RegWindow::on_exit_btn_clicked()
{
    LogOrRegWindow *w = new LogOrRegWindow();
    close();

    w->show();
}

void RegWindow::on_usname_lin_textChanged(const QString &arg1)
{
    if(ui->usname_lin->text() != "" && ui->pas_line->text() != "" && ui->name_line->text() != "" && ui->sur_line->text() != "")
        ui->reg_btn->setEnabled(true);
    else ui->reg_btn->setEnabled(false);
}

void RegWindow::on_pas_line_textChanged(const QString &arg1)
{
    if(ui->usname_lin->text() != "" && ui->pas_line->text() != "" && ui->name_line->text() != "" && ui->sur_line->text() != "")
        ui->reg_btn->setEnabled(true);
    else ui->reg_btn->setEnabled(false);
}

void RegWindow::on_name_line_textChanged(const QString &arg1)
{
    if(ui->usname_lin->text() != "" && ui->pas_line->text() != "" && ui->name_line->text() != "" && ui->sur_line->text() != "")
        ui->reg_btn->setEnabled(true);
    else ui->reg_btn->setEnabled(false);
}

void RegWindow::on_sur_line_textChanged(const QString &arg1)
{
    if(ui->usname_lin->text() != "" && ui->pas_line->text() != "" && ui->name_line->text() != "" && ui->sur_line->text() != "")
        ui->reg_btn->setEnabled(true);
    else ui->reg_btn->setEnabled(false);
}
