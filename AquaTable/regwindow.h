#ifndef REGWINDOW_H
#define REGWINDOW_H

#include <QMainWindow>
#include <databasemanager.h>
#include "logorregwindow.h"

namespace Ui {
class RegWindow;
}

class RegWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit RegWindow(QWidget *parent = 0);
    ~RegWindow();

private slots:
    void on_reg_btn_clicked();

    void on_exit_btn_clicked();

    void on_usname_lin_textChanged(const QString &arg1);

    void on_pas_line_textChanged(const QString &arg1);

    void on_name_line_textChanged(const QString &arg1);

    void on_sur_line_textChanged(const QString &arg1);

private:
    Ui::RegWindow *ui;
    DatabaseManager *_db;
signals:
    void sendClient(Client *);
};

#endif // REGWINDOW_H
