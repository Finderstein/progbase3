#include "mainadminprofdialog.h"
#include "ui_mainadminprofdialog.h"
#include <QCryptographicHash>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

MainAdminProfDialog::MainAdminProfDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainAdminProfDialog)
{
    ui->setupUi(this);
    ui->phone_line->setValidator(new QRegExpValidator(QRegExp("[0-9]\\d{12,12}")));
    this->status = ADD;
    ui->label->setText("Add worker");
}

MainAdminProfDialog::~MainAdminProfDialog()
{
    delete ui;
}

void MainAdminProfDialog::getWorker(Worker *a)
{
    this->status = EDIT;
    ui->label->setText("Edit profile");
    this->worker = a;
    ui->user_line->setText(a->getUsername());
    ui->pas_line->setText("");
    ui->name_line->setText(a->getName());
    ui->sur_line->setText(a->getSurname());
    ui->phone_line->setText(a->getPhone());
    if(a->getEduc() == "High")
        ui->ed_box->setCurrentIndex(0);
    else if(a->getEduc() == "Middle")
        ui->ed_box->setCurrentIndex(1);
    else if(a->getEduc() == "Low")
        ui->ed_box->setCurrentIndex(2);
    else
        ui->ed_box->setCurrentIndex(3);

    if(a->getSex() == "Man")
        ui->sex_box->setCurrentIndex(0);
    else if(a->getSex() == "Woman")
        ui->sex_box->setCurrentIndex(1);
    else
        ui->sex_box->setCurrentIndex(2);

    ui->birth_dateEdit->setDate(QDate::fromString(a->getDateOfBirth(), "dd.MM.yyyy"));
    ui->empl_date->setDate(QDate::fromString(a->getDateOfEmpl(), "dd.MM.yyyy"));

    ui->salary_line->setValue(a->getSalary());
    ui->pos_line->setText(a->getPosition());

    if(a->getActive() == 0)
        ui->access_comboBox->setCurrentIndex(0);
    else if(a->getActive() == 1)
        ui->access_comboBox->setCurrentIndex(1);
    else if(a->getActive() == 2)
        ui->access_comboBox->setCurrentIndex(2);
    else
        ui->access_comboBox->setCurrentIndex(3);

    ui->workdays_line->setText(a->getWorkDays());
}

void MainAdminProfDialog::getStatus(int stat)
{
    this->status = ADD;
    ui->label->setText("Add worker");
}

void MainAdminProfDialog::on_buttonBox_accepted()
{
    Worker *w;
    if(status == EDIT)
        w = worker;
    else
        w = new Worker();

    w->setUsername(ui->user_line->text());
    if(ui->pas_line->text() != "")
    {
        QString pas = ui->pas_line->text() + "@@14f#_!";
        QByteArray hashArray = QCryptographicHash::hash(pas.toUtf8(), QCryptographicHash::Md5);
        QString passwordHash = QString(hashArray.toHex());
        w->setPassword(passwordHash);
    }

    w->setName(ui->name_line->text());
    w->setSurname(ui->sur_line->text());
    w->setPhone(ui->phone_line->text());
    w->setEduc(ui->ed_box->currentText());
    w->setSex(ui->sex_box->currentText());
    w->setDateOfBirth(ui->birth_dateEdit->text());
    w->setDateOfEmpl(ui->birth_dateEdit->text());
    w->setActive(ui->access_comboBox->currentIndex());
    w->setSalary(ui->salary_line->value());
    w->setPosition(ui->pos_line->text());
    w->setWorkDays(ui->workdays_line->text());

    if(status == EDIT)
        emit returnWorker(w);
    else
        _db->addWorker(w);
}
