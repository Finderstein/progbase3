#ifndef CLIENTEDITDIALOG_H
#define CLIENTEDITDIALOG_H

#include <QDialog>
#include <client.h>
#include <databasemanager.h>

namespace Ui {
class ClientEditDialog;
}

class ClientEditDialog : public QDialog
{
    Q_OBJECT
    Client *client;
    int status;
public:
    explicit ClientEditDialog(QWidget *parent = 0);
    ~ClientEditDialog();


private:
    Ui::ClientEditDialog *ui;
    DatabaseManager *_db;
public slots:
    void getClient(Client *);
signals:
    void returnClient(Client *);
private slots:
    void on_buttonBox_accepted();
};

#endif // CLIENTEDITDIALOG_H
