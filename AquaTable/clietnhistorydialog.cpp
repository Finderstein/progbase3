#include "clietnhistorydialog.h"
#include "ui_clietnhistorydialog.h"

ClietnHistoryDialog::ClietnHistoryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClietnHistoryDialog)
{
    ui->setupUi(this);

}

ClietnHistoryDialog::~ClietnHistoryDialog()
{
    delete ui;
}

void ClietnHistoryDialog::clientHistoryTable(Client *cl)
{
    ui->history_table->setRowCount(0);

    ui->history_table->setColumnCount(8);
    ui->history_table->setHorizontalHeaderLabels(QStringList() << "Username"
                                               << "Name"
                                               << "Surname"
                                               << "Date of visit"
                                               << "Start time visit"
                                               << "End time visit"
                                               << "Active"
                                               << "Id");

    QList<Client *> clients = _db->selectClientHistory(cl);
    for(auto c : clients)
    {
        int newRowIndex = ui->history_table->rowCount();
        ui->history_table->insertRow(newRowIndex);
        QString user = c->getUsername();
        ui->history_table->setItem(newRowIndex, 0, new QTableWidgetItem(user));
        ui->history_table->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
        ui->history_table->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
        ui->history_table->setItem(newRowIndex, 3, new QTableWidgetItem(c->getDate()));
        ui->history_table->setItem(newRowIndex, 4, new QTableWidgetItem(c->getStartTimeVisit()));
        ui->history_table->setItem(newRowIndex, 5, new QTableWidgetItem(c->getEndTimeVisit()));
        if(c->getActive() == 2)
            ui->history_table->setItem(newRowIndex, 6, new QTableWidgetItem("Active"));
        else
            ui->history_table->setItem(newRowIndex, 6, new QTableWidgetItem("Visited"));
        ui->history_table->setItem(newRowIndex, 7, new QTableWidgetItem(QString::number(c->getId())));

    }
}

void ClietnHistoryDialog::on_history_table_clicked(const QModelIndex &index)
{
    QList<QTableWidgetItem *> items = ui->history_table->selectedItems();
    if(items.size() == 0)
        ui->rem_btn->setEnabled(false);
    else
        ui->rem_btn->setEnabled(true);
}

void ClietnHistoryDialog::on_rem_btn_clicked()
{
    QList<QTableWidgetItem *> items = ui->history_table->selectedItems();
    if(items.size() != 0)
    {
        int row = ui->history_table->row(items.at(0));
        QTableWidgetItem *it = ui->history_table->takeItem(row, 7);
        int user = it->text().toInt();
        QTableWidgetItem *it1 = ui->history_table->takeItem(row, 6);
        QString act = it1->text();

        if(act == "Visited" || !_db->removeVisit(user))
        {
            ui->err_label->setText("Can`t remove!");
            ui->history_table->setItem(row, 6, it1);
            ui->history_table->setItem(row, 7, it);
            return;
        }

        ui->history_table->removeRow(row);
        ui->history_table->clearSelection();
        ui->rem_btn->setEnabled(false);

    }
}

void ClietnHistoryDialog::getClient(Client *c)
{
    this->client = c;
    clientHistoryTable(this->client);
}

void ClietnHistoryDialog::on_exit_btn_clicked()
{
    close();
}
