#ifndef LOGORREGWINDOW_H
#define LOGORREGWINDOW_H

#include <QMainWindow>

namespace Ui {
class LogOrRegWindow;
}

class LogOrRegWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LogOrRegWindow(QWidget *parent = 0);
    ~LogOrRegWindow();

private slots:


    void on_log_btn_clicked();

    void on_reg_btn_clicked();

    void on_exit_btn_clicked();

private:
    Ui::LogOrRegWindow *ui;
};

#endif // LOGORREGWINDOW_H
