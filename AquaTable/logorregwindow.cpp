#include "logorregwindow.h"
#include "ui_logorregwindow.h"
#include "loginwindow.h"
#include "regwindow.h"

LogOrRegWindow::LogOrRegWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LogOrRegWindow)
{
    ui->setupUi(this);
    QPixmap pix("../orfish_big.jpg");
    ui->fish_pix->setPixmap(pix);
}

LogOrRegWindow::~LogOrRegWindow()
{
    delete ui;
}


void LogOrRegWindow::on_log_btn_clicked()
{
    LogInWindow *w = new LogInWindow();
    hide();
    w->show();
}

void LogOrRegWindow::on_reg_btn_clicked()
{
    RegWindow *w = new RegWindow();
    hide();
    w->show();
}

void LogOrRegWindow::on_exit_btn_clicked()
{
    close();
}
