#-------------------------------------------------
#
# Project created by QtCreator 2018-05-11T16:29:17
#
#-------------------------------------------------

QT       += core gui
QT += sql
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AquaTable
TEMPLATE = app


SOURCES += main.cpp\
        loginwindow.cpp \
    mainwindow.cpp \
    worker.cpp \
    databasemanager.cpp \
    client.cpp \
    logorregwindow.cpp \
    regwindow.cpp \
    clientwindow.cpp \
    clienteditdialog.cpp \
    workdays.cpp \
    mainadminprofdialog.cpp \
    clietnhistorydialog.cpp \
    addworkdaydialog.cpp

HEADERS  += loginwindow.h \
    mainwindow.h \
    worker.h \
    databasemanager.h \
    client.h \
    logorregwindow.h \
    regwindow.h \
    clientwindow.h \
    clienteditdialog.h \
    workdays.h \
    mainadminprofdialog.h \
    clietnhistorydialog.h \
    addworkdaydialog.h

FORMS    += loginwindow.ui \
    mainwindow.ui \
    logorregwindow.ui \
    regwindow.ui \
    clientwindow.ui \
    clienteditdialog.ui \
    mainadminprofdialog.ui \
    clietnhistorydialog.ui \
    addworkdaydialog.ui
