#include "loginwindow.h"
#include "mainwindow.h"
#include "ui_loginwindow.h"
#include <QDebug>
#include <QCryptographicHash>
#include "clientwindow.h"


LogInWindow::LogInWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LogInWindow)
{
    ui->setupUi(this);
    QPixmap pix("../orfish_smal.jpg");
    ui->fish_pix->setPixmap(pix);

    QString path = "../AquaTable/login.sqlite3";
    _db = new DatabaseManager(path);
    if(!_db->open()) {
        qDebug() << "can't connect: " ;
        return;

    }
}

LogInWindow::~LogInWindow()
{
    _db->close();
    delete _db;
    delete ui;
}

void LogInWindow::on_logIn_btn_clicked()
{
    if(!_db->accExists(ui->usname_lineEdit->text()))
    {
        ui->err_label->setText("Can`t find such user!");
        return;
    }
    QString pass = ui->password_lineEdit->text() + "@@14f#_!";
    QString user = ui->usname_lineEdit->text();
    QByteArray bytes = pass.QString::toUtf8();
    if(_db->workerExists(ui->usname_lineEdit->text()))
    {
        QList<Worker *> users = _db->selectAllWorkers();
        for(auto u : users)
        {
            if(QString(QCryptographicHash::hash(bytes ,QCryptographicHash::Md5).toHex()) == u->getPassword() && user == u->getUsername())
            {
                if(u->getActive() == 3)
                {
                    ui->err_label->setText("You don`t have access.");
                    return;
                }
                close();


                MainWindow *w = new MainWindow();
                connect(this, SIGNAL(sendStatus(int)), w, SLOT(getStatus(int)));
                connect(this, SIGNAL(sendWorker(Worker*)), w, SLOT(getWorker(Worker*)));
                emit sendWorker(u);
                if(u->getActive() == 1)
                    emit sendStatus(W_USUAL_ST);
                else
                    emit sendStatus(W_ADMIN_ST);
                w->show();

            }
        }

    }
    else if(_db->clientExists(ui->usname_lineEdit->text()))
    {
        QList<Client *> users = _db->selectAllClients();
        for(auto u : users)
        {
            if(QString(QCryptographicHash::hash(bytes ,QCryptographicHash::Md5).toHex()) == u->getPassword() && user == u->getUsername())
            {
                if(u->getActive() == BLACK_ST)
                {
                   ui->err_label->setText("Sorry. Your account is blacklisted!");
                   return;
                }
                close();


                ClientWindow *c = new ClientWindow();
                connect(this, SIGNAL(sendClient(Client *)), c, SLOT(getClient(Client *)));
                emit sendClient(u);
                c->show();

            }
        }

    }

    ui->err_label->setText("Invalid password or username!");
}

void LogInWindow::on_password_lineEdit_textEdited(const QString &arg1)
{
    if(ui->usname_lineEdit->text() != "" && ui->password_lineEdit->text() != "")
    ui->logIn_btn->setEnabled(true);
    else ui->logIn_btn->setEnabled(false);
}

void LogInWindow::on_usname_lineEdit_textEdited(const QString &arg1)
{
    if(ui->usname_lineEdit->text() != "" && ui->password_lineEdit->text() != "")
    ui->logIn_btn->setEnabled(true);
    else ui->logIn_btn->setEnabled(false);
}

void LogInWindow::on_exit_btn_clicked()
{
    LogOrRegWindow *w = new LogOrRegWindow();
    close();
    _db->close();
    delete _db;
    w->show();
}
