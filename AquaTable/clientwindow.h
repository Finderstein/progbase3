#ifndef CLIENTWINDOW_H
#define CLIENTWINDOW_H

#include <QMainWindow>
#include <databasemanager.h>

namespace Ui {
class ClientWindow;
}

class ClientWindow : public QMainWindow
{
    Q_OBJECT
    Client *client;

public:
    explicit ClientWindow(QWidget *parent = 0);
    ~ClientWindow();

private:
    Ui::ClientWindow *ui;
    DatabaseManager *_db;
public slots:
    void getClient(Client *);
    void editClient(Client *);
signals:
    void sendClient(Client *);
private slots:
    void on_edit_btn_clicked();
    void on_exit_btn_clicked();
    void on_add_btn_clicked();
    void on_calendarWidget_clicked(const QDate &date);
    void on_history_btn_clicked();
};

#endif // CLIENTWINDOW_H
