#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QList>
#include "worker.h"
#include "client.h"
#include "workdays.h"
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

enum UserStatus
{
    WORKER_USER = 0,
    ADMIN_USER,
};

class DatabaseManager
{

public:
    DatabaseManager(QString & databasePath);
    QSqlDatabase _db;

    bool open();
    void close();

    QList <Worker *> selectAllWorkers();
    QList <Client *> selectAllClients();
    QList <Client *> selectAllVisits();
    QList <Client *> selectClientsAndVisits();
    QList<WorkDays *> selectAllDays();
    QList<Client *> selectClientHistory(Client *);

    Worker * selectWorker(QString user);
    Client * selectClient(QString user);
    Client * selectClient(int id);
    WorkDays * selectDay(QString date);

    bool accExists(QString acc);
    bool adminExists(QString acc);
    bool clientExists(QString acc);
    bool workerExists(QString acc);
    bool dayExists(QString date);

    bool editClient(Client *, QString userName);
    bool editVisit(int id, int status);
    bool editClientVisits(Client *);
    bool editWorker(Worker *, QString userName);
    bool editDay(WorkDays *, QString userName);

    bool removeClient(QString userName);
    bool removeVisit(int id);
    bool removeWorker(QString userName);
    bool removeDay(QString date);

    bool setClientBlaclisted(QString userName);
    bool setClientActive(QString userName);

    bool addWorker(Worker *);
};

#endif // DATABASEMANAGER_H
