#include "clienteditdialog.h"
#include "ui_clienteditdialog.h"
#include <QCryptographicHash>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

ClientEditDialog::ClientEditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClientEditDialog)
{
    ui->setupUi(this);
    ui->phone_line->setValidator(new QRegExpValidator(QRegExp("[0-9]\\d{12,12}")));
}

ClientEditDialog::~ClientEditDialog()
{
    delete ui;
}

void ClientEditDialog::getClient(Client *c)
{
    this->client = c;
    ui->usern_line->setText(client->getUsername());
    //ui->pas_label->setText(client->getPassword());
    ui->name_label->setText(client->getName());
    ui->sur_label->setText(client->getSurname());
    ui->phone_line->setText(client->getPhone());
}

void ClientEditDialog::on_buttonBox_accepted()
{
    if(ui->usern_line->text() != client->getUsername() && _db->accExists(ui->usern_line->text()))
    {
        QMessageBox::information(this, "Failed", "Such user already exists!");
        return;
    }

    Client *c = client;
    if(ui->pas_line->text() != "")
    {
        QString pas = ui->pas_line->text() + "@@14f#_!";
        QByteArray hashArray = QCryptographicHash::hash(pas.toUtf8(), QCryptographicHash::Md5);
        QString passwordHash = QString(hashArray.toHex());
        c->setPassword(passwordHash);
    }
    else
    {
       c->setPassword(client->getPassword());
    }

    c->setUsername(ui->usern_line->text());
    c->setPhone(ui->phone_line->text());
    emit returnClient(c);

}
