#include "worker.h"

Worker::Worker(QObject *parent) : QObject(parent)
{

}

void Worker::setUsername(QString username)
{
    this->username = username;
}

void Worker::setPassword(QString password)
{
    this->password = password;
}

void Worker::setName(QString name)
{
    this->name = name;
}

void Worker::setSurname(QString surname)
{
    this->surname = surname;
}

void Worker::setDateOfEmpl(QString dateOfEmpl)
{
    this->dateOfEmpl = dateOfEmpl;
}

void Worker::setWorkDays(QString workDays)
{
    this->workDays = workDays;
}

void Worker::setEduc(QString educ)
{
    this->educ = educ;
}

void Worker::setSex(QString sex)
{
    this->sex = sex;
}

void Worker::setDateOfBirth(QString dateOfBirth)
{
    this->dateOfBirth = dateOfBirth;
}

void Worker::setPhone(QString phone)
{
    this->phone = phone;
}

void Worker::setPosition(QString pos)
{
    this->position = pos;
}

void Worker::setSalary(double salary)
{
    this->salary = salary;
}

void Worker::setId(int id)
{
    this->id = id;
}

void Worker::setActive(int active)
{
    this->active = active;
}


QString Worker::getUsername()
{
    return this->username;
}

QString Worker::getPassword()
{
    return this->password;
}

QString Worker::getName()
{
    return this->name;
}

QString Worker::getSurname()
{
    return this->surname;
}

QString Worker::getDateOfEmpl()
{
    return this->dateOfEmpl;
}

QString Worker::getWorkDays()
{
    return this->workDays;
}

QString Worker::getEduc()
{
    return this->educ;
}

QString Worker::getSex()
{
    return this->sex;
}

QString Worker::getDateOfBirth()
{
    return this->dateOfBirth;
}

QString Worker::getPhone()
{
    return this->phone;
}

QString Worker::getPosition()
{
    return this->position;
}

double Worker::getSalary()
{
    return this->salary;
}

int Worker::getId()
{
    return this->id;
}

int Worker::getActive()
{
    return this->active;
}
