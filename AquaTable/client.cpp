#include "client.h"

Client::Client(QObject *parent) : QObject(parent)
{

}

void Client::setUsername(QString username)
{
    this->username = username;
}

void Client::setPassword(QString password)
{
    this->password = password;
}

void Client::setName(QString name)
{
    this->name = name;
}

void Client::setSurname(QString surname)
{
    this->surname = surname;
}

void Client::setPhone(QString phone)
{
    this->phone = phone;
}

void Client::setDate(QString date)
{
    this->dateOfVisit = date;
}

void Client::setStartTimeVisit(QString time)
{
    this->startTimeVisit = time;
}

void Client::setEndTimeVisit(QString time)
{
    this->endTimeVisit = time;
}

void Client::setId(int id)
{
    this->id = id;
}

void Client::setActive(int act)
{
    this->active = act;
}


QString Client::getUsername()
{
    return this->username;
}

QString Client::getPassword()
{
    return this->password;
}

QString Client::getName()
{
    return this->name;
}

QString Client::getSurname()
{
    return this->surname;
}

QString Client::getPhone()
{
    return this->phone;
}

QString Client::getDate()
{
    return this->dateOfVisit;
}

QString Client::getStartTimeVisit()
{
    return this->startTimeVisit;
}

QString Client::getEndTimeVisit()
{
    return this->endTimeVisit;
}

int Client::getId()
{
    return this->id;
}

int Client::getActive()
{
    return this->active;
}


