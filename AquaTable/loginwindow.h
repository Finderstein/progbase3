#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include <databasemanager.h>
#include "logorregwindow.h"

namespace Ui {
class LogInWindow;
}

class LogInWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LogInWindow(QWidget *parent = 0);
    ~LogInWindow();

private slots:
    void on_logIn_btn_clicked();

    void on_password_lineEdit_textEdited(const QString &arg1);

    void on_usname_lineEdit_textEdited(const QString &arg1);

    void on_exit_btn_clicked();

private:
    Ui::LogInWindow *ui;
    DatabaseManager *_db;
signals:
    void sendClient(Client *);
    void sendWorker(Worker *);
    void sendStatus(int stat);
};

#endif // LOGINWINDOW_H
