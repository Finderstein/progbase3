#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>

enum WorkerStatus {
    W_FIRED_ST = 0,
    W_USUAL_ST,
    W_ADMIN_ST
};

class Worker : public QObject
{
    Q_OBJECT
    QString username;
    QString password;
    QString name;
    QString surname;
    QString dateOfEmpl;
    QString workDays;
    QString educ;
    QString sex;
    QString dateOfBirth;
    QString phone;
    QString position;
    double salary;
    int id;
    int active;

public:
    explicit Worker(QObject *parent = 0);

    void setUsername(QString username);
    void setPassword(QString password);
    void setName(QString name);
    void setSurname(QString surname);
    void setDateOfEmpl(QString dateOfEmpl);
    void setWorkDays(QString workDays);
    void setEduc(QString educ);
    void setSex(QString sex);
    void setDateOfBirth(QString dateOfBirth);
    void setPhone(QString phone);
    void setPosition(QString pos);
    void setSalary(double salary);
    void setId(int id);
    void setActive(int active);

    QString getUsername();
    QString getPassword();
    QString getName();
    QString getSurname();
    QString getDateOfEmpl();
    QString getWorkDays();
    QString getEduc();
    QString getSex();
    QString getDateOfBirth();
    QString getPhone();
    QString getPosition();
    double getSalary();
    int getId();
    int getActive();

signals:

public slots:
};

#endif // WORKER_H
