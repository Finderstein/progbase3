#ifndef CLIETNHISTORYDIALOG_H
#define CLIETNHISTORYDIALOG_H

#include <QDialog>
#include <databasemanager.h>

namespace Ui {
class ClietnHistoryDialog;
}

class ClietnHistoryDialog : public QDialog
{
    Q_OBJECT
    Client *client;

public:
    explicit ClietnHistoryDialog(QWidget *parent = 0);
    ~ClietnHistoryDialog();

    void clientHistoryTable(Client *);

private slots:
    void on_history_table_clicked(const QModelIndex &index);

    void on_rem_btn_clicked();

    void on_exit_btn_clicked();

private:
    Ui::ClietnHistoryDialog *ui;
    DatabaseManager *_db;
public slots:
    void getClient(Client *);
};

#endif // CLIETNHISTORYDIALOG_H
