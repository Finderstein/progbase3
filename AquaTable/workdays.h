#ifndef WORKDAYS_H
#define WORKDAYS_H

#include <QObject>

class WorkDays : public QObject
{
    Q_OBJECT
    QString date;
    QString open;
    QString close;
    int clientsNum;
    int priceAdult;
    int priceUnder18;
    int priceUnder12;
public:
    explicit WorkDays(QObject *parent = 0);

    void setDate(QString date);
    void setOpen(QString open);
    void setClose(QString close);
    void setClNum(int num);
    void setAdult(int price);
    void setUnder18(int price);
    void setUnder12(int price);

    QString getDate();
    QString getOpen();
    QString getClose();
    int getClNum();
    int getAdult();
    int getUnder18();
    int getUnder12();

signals:

public slots:
};

#endif // WORKDAYS_H
