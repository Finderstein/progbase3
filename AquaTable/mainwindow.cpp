#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QCryptographicHash>
#include <QVariant>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include "clietnhistorydialog.h"
#include "addworkdaydialog.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPixmap pix("../orfish_smal.jpg");
    ui->pix->setPixmap(pix);

    QString path = "../AquaTable/login.sqlite3";
    _db = new DatabaseManager(path);
    if(!_db->open()) {
        qDebug() << "can't connect: " ;
        return;
    }

    this->tableStatus = CLIENTS_TABLE;
    on_clients_btn_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clientsVisitTable(QString search, int table)
{
    ui->tableWidget->setRowCount(0);

    ui->tableWidget->setColumnCount(8);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Username"
                                               << "Name"
                                               << "Surname"
                                               << "Phone"
                                               << "Date of visit"
                                               << "Start time visit"
                                               << "End time visit"
                                               << "Id");

    bool find = true;
    bool visit = false;

    QList<Client *> clients = _db->selectAllVisits();
    for(auto c : clients)
    {      
        Client *cl = _db->selectClient(c->getUsername());
        if(table == 0)
        {
            if(c->getActive() == VISIT_ACTIVE_ST && cl->getActive() != BLACK_ST)
            {
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(c->getDate()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(c->getStartTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(c->getEndTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(QString::number(c->getId())));

            }
        }
        else if(table == 1)
        {
            find = false;
            if(c->getActive() == VISIT_ACTIVE_ST && cl->getActive() != BLACK_ST && c->getUsername() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(c->getDate()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(c->getStartTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(c->getEndTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(QString::number(c->getId())));

            }
        }
        else if(table == 2)
        {
            find = false;
            if(c->getActive() == VISIT_ACTIVE_ST && cl->getActive() != BLACK_ST && c->getSurname() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(c->getDate()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(c->getStartTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(c->getEndTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(QString::number(c->getId())));

            }
        }
        else if(table == 3)
        {
            find = false;
            if(c->getActive() == VISIT_ACTIVE_ST && cl->getActive() != BLACK_ST && c->getDate() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(c->getDate()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(c->getStartTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(c->getEndTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(QString::number(c->getId())));

            }
        }
        else if(table == 4)
        {
            find = false;
            if(c->getActive() == VISIT_ACTIVE_ST && cl->getActive() != BLACK_ST && c->getPhone() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(c->getDate()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(c->getStartTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(c->getEndTimeVisit()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(QString::number(c->getId())));

            }
        }

    }
    if(!find && !visit)
    {
        clientsVisitTable("", 0);
        QMessageBox::information(this, "Error search", "Can`t find such visit");

        ui->search_line->setText("");
    }
}

void MainWindow::blackClientsTable(QString search, int table)
{
    ui->tableWidget->setRowCount(0);

    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Username"
                                               << "Name"
                                               << "Surname"
                                               << "Phone");

    bool find = true;
    bool visit = false;

    QList<Client *> clients = _db->selectAllClients();
    for(auto c : clients)
    {
        if(table == 0)
        {
            if(c->getActive() == BLACK_ST)
            {
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
            }
        }
        else if(table == 1)
        {
            find = false;
            if(c->getActive() == BLACK_ST && c->getUsername() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
            }
        }
        else if(table == 2)
        {
            find = false;
            if(c->getActive() == BLACK_ST && c->getSurname() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
            }
        }
        else if(table == 3)
        {
            find = false;
            if(c->getActive() == BLACK_ST && c->getPhone() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(c->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(c->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(c->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(c->getPhone()));
            }
        }

    }
    if(!find && !visit)
    {
        blackClientsTable("", 0);
        QMessageBox::information(this, "Error search", "Can`t find such client");

        ui->search_line->setText("");
    }
}

void MainWindow::workDaysTable(QString search, int table)
{
    ui->tableWidget->setRowCount(0);

    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Date"
                                               << "Number of clients"
                                               << "Open time"
                                               << "Close time"
                                               << "Price: adults"
                                               << "Price: under18"
                                               << "Price: under12");

    bool find = true;
    bool visit = false;

    QList<WorkDays *> days = _db->selectAllDays();
    for(auto d : days)
    {
        if(table == 0)
        {
            int newRowIndex = ui->tableWidget->rowCount();
            ui->tableWidget->insertRow(newRowIndex);

            ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(d->getDate()));
            ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(QString::number(d->getClNum())));
            ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(d->getOpen()));
            ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(d->getClose()));
            ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(QString::number(d->getAdult())));
            ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(d->getUnder18())));
            ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(QString::number(d->getUnder12())));
        }
        else if(table == 1)
        {
            find = false;
            if(d->getDate() == search)
            {

                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(d->getDate()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(QString::number(d->getClNum())));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(d->getOpen()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(d->getClose()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(QString::number(d->getAdult())));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(d->getUnder18())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(QString::number(d->getUnder12())));
            }
        }
    }
    if(!find && !visit)
    {
        workDaysTable("", 0);
        QMessageBox::information(this, "Error search", "Can`t find such day");

        ui->search_line->setText("");
    }
}

void MainWindow::workersTable(int stat, QString search, int table)
{
    ui->tableWidget->setRowCount(0);

    ui->tableWidget->setColumnCount(12);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Username"
                                               << "Name"
                                               << "Surname"
                                               << "Date of birth"
                                               << "Phone"
                                               << "Salary"
                                               << "Date of employment"
                                               << "Workdays"
                                               << "Education"
                                               << "Sex"
                                               << "Position"
                                               << "Access");

    bool find = true;
    bool visit = false;

    QList<Worker *> workers = _db->selectAllWorkers();
    for(auto w : workers)
    {
        if(table == 0)
        {
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST)
                {
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST)
            {
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 1)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getUsername() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getUsername() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 2)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getSurname() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getSurname() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 3)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getDateOfBirth() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getDateOfBirth() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 4)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getDateOfEmpl() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getDateOfEmpl() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 5)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getSalary() == search.toDouble())
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getSalary() == search.toDouble())
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 6)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getEduc() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getEduc() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 7)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getSex() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getSex() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }
        else if(table == 8)
        {
            find = false;
            if(stat == W_FIRED_ST)
            {
                if(w->getActive() == W_FIRED_ST && w->getPhone() == search)
                {

                    visit = true;
                    int newRowIndex = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(newRowIndex);

                    ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                    ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                    ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                    ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                    ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                    ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                    ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                    ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                    ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                    ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                    ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
                }
            }
            else if(w->getActive() != W_FIRED_ST && w->getPhone() == search)
            {
                visit = true;
                int newRowIndex = ui->tableWidget->rowCount();
                ui->tableWidget->insertRow(newRowIndex);

                ui->tableWidget->setItem(newRowIndex, 0, new QTableWidgetItem(w->getUsername()));
                ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(w->getName()));
                ui->tableWidget->setItem(newRowIndex, 2, new QTableWidgetItem(w->getSurname()));
                ui->tableWidget->setItem(newRowIndex, 3, new QTableWidgetItem(w->getDateOfBirth()));
                ui->tableWidget->setItem(newRowIndex, 4, new QTableWidgetItem(w->getPhone()));
                ui->tableWidget->setItem(newRowIndex, 5, new QTableWidgetItem(QString::number(w->getSalary())));
                ui->tableWidget->setItem(newRowIndex, 6, new QTableWidgetItem(w->getDateOfEmpl()));
                ui->tableWidget->setItem(newRowIndex, 7, new QTableWidgetItem(w->getWorkDays()));
                ui->tableWidget->setItem(newRowIndex, 8, new QTableWidgetItem(w->getEduc()));
                ui->tableWidget->setItem(newRowIndex, 9, new QTableWidgetItem(w->getSex()));
                ui->tableWidget->setItem(newRowIndex, 10, new QTableWidgetItem(w->getPosition()));
                if(w->getActive() == 1)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Normal"));
                else if(w->getActive() == 2)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Admin"));
                else if(w->getActive() == 3)
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("None"));
                else
                    ui->tableWidget->setItem(newRowIndex, 11, new QTableWidgetItem("Fired"));
            }
        }

    }
    if(!find && !visit)
    {
        workersTable(stat, "", 0);
        QMessageBox::information(this, "Error search", "Can`t find such worker");

        ui->search_line->setText("");
    }
}

void MainWindow::on_black_btn_clicked()
{
    ui->btn1->setEnabled(false);
    ui->btn1->setText("Remove");
    ui->btn2->setEnabled(false);
    ui->btn2->setText("History");
    ui->btn3->setEnabled(false);
    ui->btn3->setText("Delete");
    ui->btn3->setFlat(false);
    ui->search_line->setText("");
    tableStatus = BLACK_TABLE;

    ui->tableWidget->clear();
    blackClientsTable("", 0);

    int count = ui->search_comboBox->count();
    for(int i = count; i > 0; i--)
    {
        ui->search_comboBox->removeItem(0);
    }
    ui->search_comboBox->addItems({"Username", "Surname", "Phone"});
}

void MainWindow::on_clients_btn_clicked()
{
    ui->btn1->setEnabled(false);
    ui->btn1->setText("Add to\nblacklist");
    ui->btn2->setEnabled(false);
    ui->btn2->setText("History");
    ui->btn3->setEnabled(false);
    ui->btn3->setText("Set\nvisited");
    ui->btn3->setFlat(false);
    ui->search_line->setText("");
    tableStatus = CLIENTS_TABLE;

    ui->tableWidget->clear();
    clientsVisitTable("", 0);

    int count = ui->search_comboBox->count();
    for(int i = count; i > 0; i--)
    {
        ui->search_comboBox->removeItem(0);
    }
    ui->search_comboBox->addItems({"Username", "Surname", "Date", "Phone"});
}

void MainWindow::on_workDays_btn_clicked()
{
    ui->btn1->setEnabled(true);
    ui->btn1->setText("Add");
    ui->btn2->setEnabled(false);
    ui->btn2->setText("Set price");
    ui->btn3->setEnabled(false);
    ui->btn3->setText("Delete");
    ui->btn3->setFlat(false);
    ui->search_line->setText("");
    tableStatus = DAYS_TABLE;
    if(userStatus == W_USUAL_ST)
    {
        ui->btn1->setEnabled(false);
    }
    ui->tableWidget->clear();
    workDaysTable("", 0);

    int count = ui->search_comboBox->count();
    for(int i = count; i > 0; i--)
    {
        ui->search_comboBox->removeItem(0);
    }
    ui->search_comboBox->addItems({"Date"});

}

void MainWindow::on_workers_btn_clicked()
{
    ui->btn1->setEnabled(true);
    ui->btn1->setText("Add");
    ui->btn2->setEnabled(false);
    ui->btn2->setText("Edit");
    ui->btn3->setEnabled(false);
    ui->btn3->setText("Fire");
    ui->btn3->setFlat(false);
    ui->search_line->setText("");
    tableStatus = WORKERS_TABLE;
    if(userStatus == W_USUAL_ST)
    {
        ui->btn1->setEnabled(false);
    }
    workersTable(W_USUAL_ST, "", 0);

    int count = ui->search_comboBox->count();
    for(int i = count; i > 0; i--)
    {
        ui->search_comboBox->removeItem(0);
    }
    ui->search_comboBox->addItems({"Username", "Surname", "Date of birth", "Date of empl", "Salary", "Education", "Sex", "Phone"});
}


void MainWindow::on_fired_btn_clicked()
{
    ui->btn1->setEnabled(false);
    ui->btn1->setText("Delete");
    ui->btn2->setEnabled(false);
    ui->btn2->setText("Edit");
    ui->btn3->setEnabled(false);
    ui->btn3->setText("");
    ui->btn3->setFlat(true);
    ui->search_line->setText("");
    tableStatus = FIRED_TABLE;

    workersTable(W_FIRED_ST, "", 0);

    int count = ui->search_comboBox->count();
    for(int i = count; i > 0; i--)
    {
        ui->search_comboBox->removeItem(0);
    }
    ui->search_comboBox->addItems({"Username", "Surname", "Date of birth", "Date of empl", "Salary", "Education", "Sex", "Phone"});
}

void MainWindow::getStatus(int stat)
{
    this->userStatus = stat;
}

void MainWindow::getWorker(Worker *user)
{
    this->userWorker = user;
}

void MainWindow::editUserWorker(Worker *w)
{
    if(w->getUsername() != this->userWorker->getUsername() && _db->accExists(w->getUsername()))
    {
        QMessageBox::information(this, "Edit error", "User with such username already exists");
        return;
    }

    if(!_db->editWorker(w, this->userWorker->getUsername()))
    {
        QMessageBox::information(this, "Edit error", "Can`t connect to database");
        return;
    }
    this->userWorker = w;
}

void MainWindow::editWorker(Worker *w)
{
    QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
    int row = ui->tableWidget->row(items.at(0));
    QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
    ui->tableWidget->setItem(row, 0, it);
    QString user = it->text();
    Worker *worker = _db->selectWorker(user);

    if(w->getUsername() != worker->getUsername() && _db->accExists(w->getUsername()))
    {
        QMessageBox::information(this, "Edit error", "User with such username already exists");
        return;
    }

    if(!_db->editWorker(w, worker->getUsername()))
    {
        QMessageBox::information(this, "Edit error", "Can`t connect to database");
        return;
    }
}

void MainWindow::on_profile_btn_clicked()
{
    MainAdminProfDialog dialog;
    connect(this, SIGNAL(sendWorker(Worker*)), &dialog, SLOT(getWorker(Worker*)));
    connect(&dialog, SIGNAL(returnWorker(Worker*)), this, SLOT(editUserWorker(Worker*)));
    emit sendWorker(this->userWorker);
    dialog.exec();
    if(tableStatus == WORKERS_TABLE)
    {
        workersTable(W_USUAL_ST, "", 0);
    }
}

void MainWindow::on_exit_btn_clicked()
{
    LogInWindow *w = new LogInWindow();
    close();

    w->show();
}



void MainWindow::on_btn1_clicked()
{
    ui->btn1->setEnabled(false);
    ui->btn2->setEnabled(false);
    ui->btn3->setEnabled(false);
    if(tableStatus == CLIENTS_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            if(!_db->setClientBlaclisted(user))
            {
                QMessageBox::information(this, "Blacklist error", "Can`t set client blacklisted");
                ui->tableWidget->setItem(row, 0, it);
                return;
            }

            clientsVisitTable("", 0);

        }
    }
    else if(tableStatus == BLACK_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            if(!_db->setClientActive(user))
            {
                QMessageBox::information(this, "Blacklist error", "Can`t set client active");
                ui->tableWidget->setItem(row, 0, it);
                return;
            }

            blackClientsTable("", 0);
        }
    }
    else if(tableStatus == DAYS_TABLE)
    {
        ui->btn1->setEnabled(true);
        AddWorkdayDialog dialog;

        dialog.exec();

        workDaysTable("", 0);
    }
    else if(tableStatus == WORKERS_TABLE)
    {
        MainAdminProfDialog dialog;
        dialog.exec();
        workersTable(W_USUAL_ST, "", 0);
    }
    else if(tableStatus == FIRED_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();

            _db->removeWorker(user);

            workersTable(W_FIRED_ST, "", 0);

        }
    }


}

void MainWindow::on_btn2_clicked()
{
    ui->btn1->setEnabled(false);
    ui->btn2->setEnabled(false);
    ui->btn3->setEnabled(false);
    if(tableStatus == CLIENTS_TABLE || tableStatus == BLACK_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            Client *cl = _db->selectClient(user);

            ClietnHistoryDialog dialog;
            connect(this, SIGNAL(sendClient(Client*)), &dialog, SLOT(getClient(Client*)));
            emit sendClient(cl);
            dialog.exec();

            if(tableStatus == CLIENTS_TABLE)
                clientsVisitTable("", 0);
            else
                blackClientsTable("", 0);

        }
    }
    else if(tableStatus == DAYS_TABLE)
    {
        ui->btn1->setEnabled(true);
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            AddWorkdayDialog dialog;
            connect(this, SIGNAL(sendWorkday(WorkDays*)), &dialog, SLOT(getWorkday(WorkDays*)));

            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString date = it->text();
            WorkDays *day = _db->selectDay(date);

            emit sendWorkday(day);
            dialog.exec();

            workDaysTable("", 0);
        }
    }
    else if(tableStatus == WORKERS_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            Worker *w = _db->selectWorker(user);


            MainAdminProfDialog dialog;
            connect(this, SIGNAL(sendWorker(Worker*)), &dialog, SLOT(getWorker(Worker*)));
            connect(&dialog, SIGNAL(returnWorker(Worker*)), this, SLOT(editWorker(Worker*)));
            emit sendWorker(w);
            dialog.exec();
            workersTable(W_USUAL_ST, "", 0);
        }

    }
    else if(tableStatus == FIRED_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            Worker *w = _db->selectWorker(user);

            MainAdminProfDialog dialog;
            connect(this, SIGNAL(sendWorker(Worker*)), &dialog, SLOT(getWorker(Worker*)));
            connect(&dialog, SIGNAL(returnWorker(Worker*)), this, SLOT(editWorker(Worker*)));
            emit sendWorker(w);
            dialog.exec();
            workersTable(W_FIRED_ST, "", 0);

        }
    }
}

void MainWindow::on_btn3_clicked()
{
    ui->btn1->setEnabled(false);
    ui->btn2->setEnabled(false);
    ui->btn3->setEnabled(false);
    if(tableStatus == CLIENTS_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 7);
            ui->tableWidget->setItem(row, 7, it);
            int id = it->text().toInt();
            _db->editVisit(id, VISIT_UNACTIVE_ST);

            clientsVisitTable("", 0);

        }
    }
    else if(tableStatus == BLACK_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            Client *cl = _db->selectClient(user);
            QList<Client *> clients = _db->selectClientHistory(cl);
            for(auto c : clients)
            {
                _db->removeClient(c->getUsername());
            }

            _db->removeClient(user);

            blackClientsTable("", 0);

        }
    }
    else if(tableStatus == DAYS_TABLE)
    {
        ui->btn1->setEnabled(true);
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString date = it->text();
            _db->removeDay(date);

            workDaysTable("", 0);
        }
    }
    else if(tableStatus == WORKERS_TABLE)
    {
        QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
        if(items.size() != 0)
        {
            int row = ui->tableWidget->row(items.at(0));
            QTableWidgetItem *it = ui->tableWidget->takeItem(row, 0);
            ui->tableWidget->setItem(row, 0, it);
            QString user = it->text();
            Worker *w = _db->selectWorker(user);
            w->setActive(W_FIRED_ST);
            _db->editWorker(w, user);

            workersTable(W_USUAL_ST, "", 0);
        }

    }

}

void MainWindow::on_tableWidget_clicked(const QModelIndex &index)
{
    QList<QTableWidgetItem *> items = ui->tableWidget->selectedItems();
    if(userStatus == W_ADMIN_ST)
        if(tableStatus == FIRED_TABLE)
        {

            if(items.size() == 0)
            {
                ui->btn1->setEnabled(false);
                ui->btn2->setEnabled(false);
                ui->btn3->setEnabled(false);
            }
            else
            {
                ui->btn1->setEnabled(true);
                ui->btn2->setEnabled(true);
                ui->btn3->setEnabled(false);
            }
        }
        else
        {
            if(items.size() == 0)
            {
                ui->btn1->setEnabled(false);
                ui->btn2->setEnabled(false);
                ui->btn3->setEnabled(false);
            }
            else
            {
                ui->btn1->setEnabled(true);
                ui->btn2->setEnabled(true);
                ui->btn3->setEnabled(true);
            }
        }
    else
        if(tableStatus == DAYS_TABLE || tableStatus == WORKERS_TABLE || tableStatus == FIRED_TABLE)
        {
            ui->btn1->setEnabled(false);
            ui->btn2->setEnabled(false);
            ui->btn3->setEnabled(false);
        }
        else
        {
            if(items.size() == 0)
            {
                ui->btn1->setEnabled(false);
                ui->btn2->setEnabled(false);
                ui->btn3->setEnabled(false);
            }
            else
            {
                ui->btn1->setEnabled(true);
                ui->btn2->setEnabled(true);
                ui->btn3->setEnabled(true);
            }
        }

}


void MainWindow::on_search_btn_clicked()
{
    if(ui->search_line->text() != "")
    {
        if(tableStatus == CLIENTS_TABLE)
        {
            clientsVisitTable(ui->search_line->text(), ui->search_comboBox->currentIndex() + 1);
        }
        else if(tableStatus == BLACK_TABLE)
        {
            blackClientsTable(ui->search_line->text(), ui->search_comboBox->currentIndex() + 1);
        }
        else if(tableStatus == DAYS_TABLE)
        {
            workDaysTable(ui->search_line->text(), ui->search_comboBox->currentIndex() + 1);
        }
        else if(tableStatus == WORKERS_TABLE)
        {
            workersTable(W_USUAL_ST, ui->search_line->text(), ui->search_comboBox->currentIndex() + 1);
        }
        else if(tableStatus == FIRED_TABLE)
        {
            workersTable(W_FIRED_ST, ui->search_line->text(), ui->search_comboBox->currentIndex() + 1);
        }
    }
}
