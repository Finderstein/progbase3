#include "databasemanager.h"
#include <QDebug>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

DatabaseManager::DatabaseManager(QString & databaseName)
{
    this->_db = QSqlDatabase::addDatabase("QSQLITE");
    this->_db.setDatabaseName(databaseName);
}

bool DatabaseManager::open() {
    return this->_db.open();
}

void DatabaseManager::close() {
    _db.removeDatabase("QSQLITE");
    this->_db.close();
}

static Worker * workerFromQuery(QSqlQuery & query) {
    Worker * w = new Worker();

    w->setUsername(query.value("Username").toString());
    w->setPassword(query.value("Password").toString());
    w->setName(query.value("Name").toString());
    w->setSurname(query.value("Surname").toString());
    w->setDateOfEmpl(query.value("Date_of_employment").toString());
    w->setWorkDays(query.value("Workdays").toString());
    w->setEduc(query.value("Education").toString());
    w->setSex(query.value("Sex").toString());
    w->setDateOfBirth(query.value("Date_of_birth").toString());
    w->setPhone(query.value("Phone").toString());
    w->setPosition(query.value("Position").toString());
    w->setSalary(query.value("Salary").toDouble());
    w->setId(query.value("Id").toInt());
    w->setActive(query.value("Active").toInt());

    return w;
}

QList<Worker *> DatabaseManager::selectAllWorkers() {
    QList<Worker *> workers;
    QSqlQuery query("SELECT * FROM Workers");

    while(query.next())
    {
        Worker * w = workerFromQuery(query);
        workers.push_back(w);
    }

    return workers;
}

static Client * clientFromQuery(QSqlQuery & query) {
    Client * c = new Client();

    c->setUsername(query.value("Username").toString());
    c->setPassword(query.value("Password").toString());
    c->setName(query.value("Name").toString());
    c->setSurname(query.value("Surname").toString());
    c->setDate(query.value("Date_of_visit").toString());
    c->setStartTimeVisit(query.value("Start_time_visit").toString());
    c->setEndTimeVisit(query.value("End_time_visit").toString());
    c->setPhone(query.value("Phone").toString());
    c->setId(query.value("Id").toInt());
    c->setActive(query.value("Active").toInt());

    return c;
}

QList<Client *> DatabaseManager::selectAllClients()
{
    QList<Client *> clients;
    QSqlQuery query("SELECT * FROM Clients");

    while(query.next())
    {
        Client * c = clientFromQuery(query);
        if(c->getActive() == ACTIVE_ST || c->getActive() == BLACK_ST)
        clients.push_back(c);
    }

    return clients;
}

QList<Client *> DatabaseManager::selectAllVisits()
{
    QList<Client *> clients;
    QSqlQuery query("SELECT * FROM Clients Where Active = \"2\" OR Active = \"3\"");

    while(query.next())
    {
        Client * c = clientFromQuery(query);
        clients.push_back(c);
    }

    return clients;
}

QList<Client *> DatabaseManager::selectClientsAndVisits()
{
    QList<Client *> clients;
    QSqlQuery query("SELECT * FROM Clients");

    while(query.next())
    {
        Client * c = clientFromQuery(query);
        clients.push_back(c);
    }

    return clients;
}

static WorkDays * dayFromQuery(QSqlQuery & query) {
    WorkDays * d = new WorkDays();

    d->setDate(query.value("Date").toString());
    d->setClNum(query.value("ClientsNum").toInt());
    d->setOpen(query.value("StartTime").toString());
    d->setClose(query.value("EndTime").toString());
    d->setAdult(query.value("Price_adult").toInt());
    d->setUnder18(query.value("Price_under18").toInt());
    d->setUnder12(query.value("Price_under12").toInt());

    return d;
}

QList<WorkDays *> DatabaseManager::selectAllDays()
{
    QList<WorkDays *> days;
    QSqlQuery query("SELECT * FROM WorkDays");

    while(query.next())
    {
        WorkDays * d = dayFromQuery(query);
        days.push_back(d);
    }

    return days;
}

QList<Client *> DatabaseManager::selectClientHistory(Client *cl)
{
    QList<Client *> clients;
    QSqlQuery query("SELECT * FROM Clients");

    while(query.next())
    {
        Client * c = clientFromQuery(query);
        QString user = c->getUsername();
        if(cl->getUsername() == user && (c->getActive() == VISIT_ACTIVE_ST || c->getActive() == VISIT_UNACTIVE_ST))
        clients.push_back(c);
    }

    return clients;
}

Worker *DatabaseManager::selectWorker(QString user)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Workers WHERE Username = :user");
    query.bindValue(":user", user);
    query.exec();
    query.first();
    return workerFromQuery(query);

}

Client *DatabaseManager::selectClient(QString user)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Clients WHERE Username = :user");
    query.bindValue(":user", user);
    query.exec();

    while(query.next())
    {
        Client *c = clientFromQuery(query);
        if(c->getActive() == ACTIVE_ST || c->getActive() == BLACK_ST)
            return c;
    }

}

Client *DatabaseManager::selectClient(int id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Clients WHERE Id = :id");
    query.bindValue(":id", id);
    query.exec();
    query.first();

    return clientFromQuery(query);
}


WorkDays *DatabaseManager::selectDay(QString date)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Workdays WHERE Date = :date");
    query.bindValue(":date", date);
    query.exec();
    query.first();
    return dayFromQuery(query);
}


bool DatabaseManager::accExists(QString userName)
{
    if(workerExists(userName))
    {
        return true;
    }
    else if(clientExists(userName))
    {
        return true;
    }
    else return false;
}


bool DatabaseManager::clientExists(QString userName)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Clients WHERE Username = :user");
    query.bindValue(":user", userName);
    query.exec();
    if(query.next())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool DatabaseManager::workerExists(QString userName)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Workers WHERE Username = :user");
    query.bindValue(":user", userName);
    query.exec();
    if(query.next())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool DatabaseManager::dayExists(QString date)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM WorkDays WHERE Date = :date");
    query.bindValue(":date", date);
    query.exec();
    if(query.next())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool DatabaseManager::editClient(Client *c, QString userName)
{
    QSqlQuery query;
    query.prepare("UPDATE Clients SET Username = :user, Password = :pas, Name = :name, Surname = :sur, Date_of_visit = :date, Start_time_visit = :start, End_time_visit = :end, Active = :stat, Phone = :phone  WHERE Username = :setuser AND Active = \"1\"");
    query.bindValue(":setuser", userName);
    query.bindValue(":user", c->getUsername());
    query.bindValue(":pas", c->getPassword());
    query.bindValue(":name", c->getName());
    query.bindValue(":sur", c->getSurname());
    query.bindValue(":date", c->getDate());
    query.bindValue(":start", c->getStartTimeVisit());
    query.bindValue(":end", c->getEndTimeVisit());
    query.bindValue(":phone", c->getPhone());
    query.bindValue(":stat", c->getActive());
    if(query.exec())
        return true;
    else
    {
        QString err = query.lastError().text();
        return false;
    }

}

bool DatabaseManager::editVisit(int id, int status)
{
    QSqlQuery query;
    query.prepare("UPDATE Clients SET Active = :stat WHERE Id = :id");
    query.bindValue(":id", id);
    query.bindValue(":stat", status);
    if(query.exec())
        return true;
    else
    {
        QString err = query.lastError().text();
        return false;
    }
}

bool DatabaseManager::editClientVisits(Client *c)
{
    QSqlQuery query;
    query.prepare("UPDATE Clients SET Username = :user, Password = :pas, Phone = :phone  WHERE Username = :setuser AND (Active = \"2\" OR Active = \"3\")");
    query.bindValue(":setuser", c->getUsername());
    query.bindValue(":user", c->getUsername());
    query.bindValue(":pas", c->getPassword());
    query.bindValue(":phone", c->getPhone());
    if(query.exec())
        return true;
    else
    {
        QString err = query.lastError().text();
        return false;
    }
}

bool DatabaseManager::editWorker(Worker *w, QString userName)
{
    QSqlQuery query;
    query.prepare("UPDATE Workers SET Username = :user, Password = :pas, Name = :name, Surname = :sur, Date_of_birth = :date, Active = :stat, Phone = :phone, Salary = :sal, Date_of_employment = :empl, Workdays = :days, Education = :ed, Sex = :sex, Position = :pos  WHERE Username = :setuser");
    query.bindValue(":setuser", userName);
    query.bindValue(":user", w->getUsername());
    query.bindValue(":pas", w->getPassword());
    query.bindValue(":name", w->getName());
    query.bindValue(":sur", w->getSurname());
    query.bindValue(":date", w->getDateOfBirth());
    query.bindValue(":sal", w->getSalary());
    query.bindValue(":empl", w->getDateOfEmpl());
    query.bindValue(":phone", w->getPhone());
    query.bindValue(":stat", w->getActive());
    query.bindValue(":sex", w->getSex());
    query.bindValue(":ed", w->getEduc());
    query.bindValue(":days", w->getWorkDays());
    query.bindValue(":pos", w->getPosition());
    if(query.exec())

        return true;
    else
        QString str = query.lastError().text();
        return false;
}

bool DatabaseManager::editDay(WorkDays *d, QString day)
{
    QSqlQuery query;
    query.prepare("UPDATE WorkDays SET Date = :date, ClientsNum = :num, StartTime = :start, EndTime = :end, Price_adult = :adult, Price_under18 = :under18, Price_under12 = :under12 WHERE Date = :setdate");
    query.bindValue(":setdate", day);
    query.bindValue(":date", d->getDate());
    query.bindValue(":num", d->getClNum());
    query.bindValue(":start", d->getOpen());
    query.bindValue(":end", d->getClose());
    query.bindValue(":adult", d->getAdult());
    query.bindValue(":under18", d->getUnder18());
    query.bindValue(":under12", d->getUnder12());
    if(query.exec())
        return true;
    else
      return false;
}

bool DatabaseManager::removeClient(QString userName)
{
    QSqlQuery query;
    query.prepare("DELETE FROM Clients Where Username = :user");
    query.bindValue(":user", userName);
    if(query.exec())
        return true;
    else
        return false;
}

bool DatabaseManager::removeVisit(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM Clients Where Id = :id");
    query.bindValue(":id", id);
    if(query.exec())
        return true;
    else
        return false;
}

bool DatabaseManager::removeWorker(QString userName)
{
    QSqlQuery query;
    query.prepare("DELETE FROM Workers Where Username = :user");
    query.bindValue(":user", userName);
    if(query.exec())
        return true;
    else
      return false;
}

bool DatabaseManager::removeDay(QString date)
{
    QSqlQuery query;
    query.prepare("DELETE FROM WorkDays Where Date = :date");
    query.bindValue(":date", date);
    if(query.exec())
        return true;
    else
        return false;
}

bool DatabaseManager::setClientBlaclisted(QString userName)
{
    QSqlQuery query;
    query.prepare("UPDATE Clients SET Active = :black Where Username = :user AND Active = \"1\"");
    query.bindValue(":user", userName);
    query.bindValue(":black", BLACK_ST);
    if(query.exec())
        return true;
    else
        return false;
}

bool DatabaseManager::setClientActive(QString userName)
{
    QSqlQuery query;
    query.prepare("UPDATE Clients SET Active = :black Where Username = :user AND Active = \"0\"");
    query.bindValue(":user", userName);
    query.bindValue(":black", ACTIVE_ST);
    if(query.exec())
        return true;
    else
        return false;
}

bool DatabaseManager::addWorker(Worker *w)
{
    QSqlQuery query;
    query.prepare("INSERT INTO Workers (Username, Password, Name, Surname, Date_of_birth, Date_of_employment, Phone, Active, Salary, Position, Education, Sex, Workdays) "
                  "VALUES (:login, :pas, :name, :surname, :dateb, :datee, :phone, :active, :sal, :pos, :ed, :sex, :days)");
    query.bindValue(":login", w->getUsername());
    query.bindValue(":pas", w->getPassword());
    query.bindValue(":name", w->getName());
    query.bindValue(":surname", w->getSurname());
    query.bindValue(":dateb", w->getDateOfBirth());
    query.bindValue(":datee", w->getDateOfEmpl());
    query.bindValue(":phone", w->getPhone());
    query.bindValue(":active", w->getActive());
    query.bindValue(":sal", w->getSalary());
    query.bindValue(":pos", w->getPosition());
    query.bindValue(":ed", w->getEduc());
    query.bindValue(":sex", w->getSex());
    query.bindValue(":days", w->getWorkDays());
    if(query.exec())
        return true;
    else
        return false;
}
